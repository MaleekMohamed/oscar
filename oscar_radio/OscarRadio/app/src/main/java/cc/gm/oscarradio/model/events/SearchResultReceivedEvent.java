package cc.gm.oscarradio.model.events;

import java.util.ArrayList;

import cc.gm.oscarradio.model.Serial;

public class SearchResultReceivedEvent {
    ArrayList<Serial> data;

    public ArrayList<Serial> getData() {
        return data;
    }

    public void setData(ArrayList<Serial> data) {
        this.data = data;
    }
}
