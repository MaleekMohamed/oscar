package cc.gm.oscarradio.model;

public class MainCategory {

    private String id;
    private String name;
    private String icon;
    private String active;

    public MainCategory(){
    }

    @Override
    public String toString() {
        return "MainCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + icon + '\'' +
                '}';
    }

    public MainCategory(String id, String catName, String catIcon, String active) {
        this.id = id;
        this.name = catName;
        this.icon = catIcon;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
