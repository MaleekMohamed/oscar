package cc.gm.oscarradio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import cc.gm.oscarradio.utils.UIUtils;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView tvSplashTitle = (TextView) findViewById(R.id.tv_splash_title);

        UIUtils.overrideFonts(OscarApplication.getAppContext(), tvSplashTitle);

        if (savedInstanceState==null){

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, AppConstants.SPLASH_INTERVAL);
        }
    }
}


