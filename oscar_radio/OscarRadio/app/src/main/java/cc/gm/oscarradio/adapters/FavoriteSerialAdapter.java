package cc.gm.oscarradio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.Serial;
import cc.gm.oscarradio.utils.UIUtils;

public class FavoriteSerialAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<Serial> serialList;

    private ItemViewHolder itemViewHolder = null;

    public FavoriteSerialAdapter(Context context, List<Serial> data) {
        this.mContext = context;
        this.serialList = data;
    }

    @Override
    public int getCount() {
        return serialList.size();
    }

    @Override
    public Object getItem(int position) {
        return serialList.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(serialList.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final Serial item = serialList.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_favorite_serial, null);

            itemViewHolder = new ItemViewHolder();

            itemViewHolder.ivSeriesIcon = (ImageView) convertView.findViewById(R.id.iv_adapter_fav_series);
            itemViewHolder.ivfav = (ImageView) convertView.findViewById(R.id.iv_adapter_serial_fav);
            itemViewHolder.tvSeriesName = (TextView) convertView.findViewById(R.id.tv_adapter_fav_series_name);
            itemViewHolder.tvSeriesCast = (TextView) convertView.findViewById(R.id.tv_adapter_fav_serial_cast);

            convertView.setTag(itemViewHolder);
        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        if(itemViewHolder != null) {
            if (itemViewHolder.ivSeriesIcon != null) {

                if(UIUtils.isEmpty(item.getImage())) {
                    itemViewHolder.ivSeriesIcon.setImageResource(R.drawable.default_image);
                } else {
                    Picasso.with(mContext)
                            .load(item.getImage())
                            .error(R.drawable.default_image)
                            .into(itemViewHolder.ivSeriesIcon);
                }

            }

            if (itemViewHolder.ivfav != null) {
                itemViewHolder.ivfav.setImageResource(R.drawable.favourited);

                itemViewHolder.ivfav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((GridView) parent).performItemClick(v, position, 0);
                    }
                });
            }

            if (itemViewHolder.tvSeriesName != null)
                itemViewHolder.tvSeriesName.setText(item.getName());

            if (itemViewHolder.tvSeriesCast != null)
                itemViewHolder.tvSeriesCast.setText(item.getCast());
        }


        UIUtils.setImageClickEffect(itemViewHolder.ivfav);

        UIUtils.overrideFonts(mContext, convertView);

        return convertView;
    }

    static class ItemViewHolder {
        public ImageView ivSeriesIcon;
        public ImageView ivfav;
        public TextView tvSeriesName;
        public TextView tvSeriesCast;
    }
}
