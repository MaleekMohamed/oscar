package cc.gm.oscarradio.model.events;

import cc.gm.oscarradio.model.AudioEpisode;

public class EpisodeReceivedEvent {

    public AudioEpisode getEpisode() {
        return episode;
    }

    public void setEpisode(AudioEpisode episode) {
        this.episode = episode;
    }

    private AudioEpisode episode;


}
