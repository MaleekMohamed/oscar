package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.MainCategoryAdapter;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.model.events.MainCategoriesErrorReceivedEvent;
import cc.gm.oscarradio.model.events.MainCategoriesReceivedEvent;
import cc.gm.oscarradio.model.MainCategory;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.GetAllCategoriesRequest;
import de.greenrobot.event.EventBus;

public class MainFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View rootView;
    private ListView lvCategories;
    private ImageView ivSearch;
    private EditText etSearch;
    private Button btnMostWatched, btnMostDownloaded, btnMostLiked;
    private ArrayList<MainCategory> data;
    MainCategoryAdapter adapter;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.title = getString(R.string.app_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_main, container,false);

        initBindViews();

        UIUtils.overrideFonts(getActivity(), rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //setupCustomActionBar(title);
        mActivityCallbacks.setupCustomActionBar(title, View.INVISIBLE, false);

        setContentShown(false);

        data = new ArrayList<MainCategory>();
        adapter = new MainCategoryAdapter(OscarApplication.getAppContext(), data);
        lvCategories.setAdapter(adapter);

        //make request to main categories
        RequestManager.getInstance(getActivity()).doRequest().getMainCategories();
    }


    private void initBindViews() {
        lvCategories = (ListView)rootView.findViewById(R.id.lv_main_categories);

        ivSearch = (ImageView) rootView.findViewById(R.id.iv_search);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);

        //llMostButtons = (LinearLayout) rootView.findViewById(R.id.ll_main_most_buttons);

        btnMostWatched = (Button) rootView.findViewById(R.id.btn_most_watched);
        btnMostDownloaded = (Button) rootView.findViewById(R.id.btn_most_downloaded);
        btnMostLiked = (Button) rootView.findViewById(R.id.btn_most_liked);

        // Set buttons effect
        UIUtils.setImageClickEffect(ivSearch);

        // Init click Listeners
        ivSearch.setOnClickListener(this);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        btnMostWatched.setOnClickListener(this);
        btnMostDownloaded.setOnClickListener(this);
        btnMostLiked.setOnClickListener(this);

        lvCategories.setOnItemClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.INVISIBLE, false);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(MainCategoriesReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(MainCategoriesErrorReceivedEvent.class);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(MainCategoriesReceivedEvent event){

        mActivityCallbacks.disableRefresh();

        data.clear();

        for (MainCategory category: event.getData()) {
            String active = category.getActive();
            if(UIUtils.isEmpty(active)) {
                data.add(category);
            } else {
                if(active.equals(AppConstants.STATE_ACTIVE)) {
                    data.add(category);
                }
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(MainCategoriesErrorReceivedEvent event){
        GetAllCategoriesRequest request = new GetAllCategoriesRequest();

        mActivityCallbacks.enableRefresh(request, AppConstants.REQUEST_CATEGORIES_TAG);

        String errorMessage = event.getErrorMessage();

        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

        setContentShown(true);
    }

    private void search() {
        //Toast.makeText(getActivity(), etSearch.getText().toString(), Toast.LENGTH_SHORT).show();
        String searchKeyword = etSearch.getText().toString();

        try {
            searchKeyword = URLEncoder.encode(searchKeyword, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(!UIUtils.isEmpty(searchKeyword)) {
            SearchResultFragment fragment = new SearchResultFragment();

            Bundle args = new Bundle();

            args.putString(AppConstants.ARG_TITLE, getString(R.string.search));
            args.putString(AppConstants.ARG_SEARCH_KEYWORK, searchKeyword);
            args.putInt(AppConstants.ARG_MOST_TYPE, AppConstants.SEARCH_SERIALS);

            fragment.setArguments(args);

            mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
        }

        etSearch.clearFocus();
        InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    private void getMostSerials(int mostType, String title) {
        SearchResultFragment fragment = new SearchResultFragment();

        Bundle args = new Bundle();

        args.putString(AppConstants.ARG_TITLE, title);
        args.putInt(AppConstants.ARG_MOST_TYPE, mostType);

        fragment.setArguments(args);

        mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.CAT_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.CAT_FRAGMENT_ID);

        MainCategory mainCategory = data.get(position);

        if(mainCategory.getId().equals(AppConstants.COMING_SOON_CATEGORY_ID)) {

            ComingSoonFragment comingSoonFragment = new ComingSoonFragment();

            Bundle args = new Bundle();
            args.putString(AppConstants.ARG_TITLE, mainCategory.getName());

            comingSoonFragment.setArguments(args);

            mActivityCallbacks.addFragment(comingSoonFragment, SerialsFragment.class.getSimpleName());

        } else {

            SubCategoriesFragment subFragment = new SubCategoriesFragment();

            Bundle args = new Bundle();
            args.putString(AppConstants.ARG_CAT_ID,data.get(position).getId());
            args.putString(AppConstants.ARG_TITLE, data.get(position).getName());

            subFragment.setArguments(args);

            mActivityCallbacks.addFragment(subFragment, SubCategoriesFragment.class.getSimpleName());

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search:
                search();
                break;
            case R.id.btn_most_watched:
                getMostSerials(AppConstants.MOST_WATCHED_SERIALS, getString(R.string.btn_most_watched));
                break;
            case R.id.btn_most_downloaded:
                getMostSerials(AppConstants.MOST_DOENLOADED_SERIALS, getString(R.string.btn_most_downloaded));
                break;
            case R.id.btn_most_liked:
                getMostSerials(AppConstants.MOST_LIKED_SERIALS, getString(R.string.btn_most_liked));
                break;
            default:
                break;
        }
    }

}
