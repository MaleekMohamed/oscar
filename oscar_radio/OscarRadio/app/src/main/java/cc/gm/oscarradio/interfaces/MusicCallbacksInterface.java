package cc.gm.oscarradio.interfaces;

import cc.gm.oscarradio.model.AudioEpisode;

public interface MusicCallbacksInterface {

    public void pauseEpisode();
    public void playEpisode(AudioEpisode episode);
    public void stopEpisode();
    public void resumeEpisode();
}
