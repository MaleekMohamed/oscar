package cc.gm.oscarradio.model.events;

import cc.gm.oscarradio.model.VideoEpisode;

public class VideoEpisodeReceivedEvent {

    private VideoEpisode episode;

    public VideoEpisode getEpisode() {
        return episode;
    }

    public void setEpisode(VideoEpisode episode) {
        this.episode = episode;
    }
}
