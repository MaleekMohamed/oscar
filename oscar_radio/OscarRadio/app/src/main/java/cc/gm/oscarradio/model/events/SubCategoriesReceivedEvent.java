package cc.gm.oscarradio.model.events;

import java.util.ArrayList;

import cc.gm.oscarradio.model.SubCategory;

public class SubCategoriesReceivedEvent {

    ArrayList<SubCategory> data;

    public ArrayList<SubCategory> getData() {
        return data;
    }

    public void setData(ArrayList<SubCategory> data) {
        this.data = data;
    }
}
