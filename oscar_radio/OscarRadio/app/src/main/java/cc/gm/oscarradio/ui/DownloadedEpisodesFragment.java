package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.DownloadedEpisodeAdapter;
import cc.gm.oscarradio.backgroundtasks.MusicService;
import cc.gm.oscarradio.database.AppDBHelper;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.interfaces.MusicCallbacksInterface;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.events.DownladAudioEpisodeErrorEvent;
import cc.gm.oscarradio.model.events.DownladVideoEpisodeErrorEvent;
import cc.gm.oscarradio.model.events.EpisodePlayingStateEvent;
import cc.gm.oscarradio.model.events.EpisodeReceivedEvent;
import cc.gm.oscarradio.model.events.VideoEpisodeReceivedEvent;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.utils.Utils;
import de.greenrobot.event.EventBus;

public class DownloadedEpisodesFragment extends ProgressFragment implements DownloadedEpisodeAdapter.AdapterCallback {

    private View rootView;
    private ListView lvEpisodes;
    private ArrayList<AudioEpisode> episodes;
    private DownloadedEpisodeAdapter adapter;

    private final int DELETE_ITEM_ID = 0;

    private AppDBHelper dbHelper;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;
    private MusicCallbacksInterface mMusicCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
            mMusicCallbacks = (MusicCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_downloaded_episodes, container,false);

        initBindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return rootView;
    }

    private void initBindViews() {
        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_downloaded_episodes);

        registerForContextMenu(lvEpisodes);

        lvEpisodes.setEmptyView(rootView.findViewById(R.id.tv_empty_view));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        if(v.getId() == R.id.lv_downloaded_episodes) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;

            AudioEpisode episode = (AudioEpisode) lv.getItemAtPosition(acmi.position);

            String[] items = getResources().getStringArray(R.array.menu_doenloaded_episodes);

            for (int i = 0; i < items.length; i++) {
                menu.add(Menu.NONE, DELETE_ITEM_ID, i, items[i]);
            }

        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final AudioEpisode episode = (AudioEpisode) adapter.getItem(info.position);

        switch (item.getItemId()) {
            case 0:
                showDialog(episode, info.position);
                break;
            default:
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void showDialog(final AudioEpisode episode, final int position) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        deleteEpisode(episode, position);
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.delete_downloaded_episode_message),
                        episode.getParentName(), episode.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void deleteEpisode(AudioEpisode episode, int position) {
        File file = new File(episode.getPath());
        if(file.exists()) {
            boolean deleted = file.delete();
        }

        dbHelper.deleteEpisode(episode.getId(), episode.getParentId());

        episodes.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentShown(false);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        episodes = new ArrayList<AudioEpisode>();
        adapter = new DownloadedEpisodeAdapter(OscarApplication.getAppContext(), episodes, this);
        lvEpisodes.setAdapter(adapter);

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        episodes.clear();
        episodes.addAll(dbHelper.getEpisodes());

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            } else {
                switch (episodes.get(i).getDownloadingState()) {
                    case AppConstants.STATE_DOWNLOADING:
                        episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADING);
                        break;
                    case AppConstants.STATE_NOT_DOWNLOADED:
                        episodes.get(i).setDownloadingState(AppConstants.STATE_NOT_DOWNLOADED);
                        break;
                    default:
                        break;
                }
            }
        }

        if(MusicService.isRunning()) {
            AudioEpisode episode = MusicService.getEpisode();
            if(episode != null) {
                int position = Utils.getItemPosition(episode, episodes);

                if(position != -1) {
                    episodes.get(position).setPlayingState(episode.getPlayingState());
                }
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SERIALS_FRAGMENT_ID);
        super.onStop();
    }

    public void onEventMainThread(EpisodePlayingStateEvent event) {
        int position = Utils.getItemPosition(event.getEpisode(), episodes);

        if(position != -1) {
            episodes.get(position).setPlayingState(event.getEpisode().getPlayingState());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(EpisodeReceivedEvent event) {
        int position = Utils.getItemPosition(event.getEpisode(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(event.getEpisode().getPath());
            episodes.get(position).setDownloadingState(event.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(event.getEpisode().getDownloadProgress());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(DownladAudioEpisodeErrorEvent errorEvent) {
        int position = Utils.getItemPosition(errorEvent.getEpisode(), episodes);

        if(position != -1) {

            episodes.remove(position);

            adapter.notifyDataSetChanged();
        }

        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    public void onEventMainThread(VideoEpisodeReceivedEvent event) {
        int position = Utils.getItemPosition(event.getEpisode().getId(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(event.getEpisode().getPath());
            episodes.get(position).setDownloadingState(event.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(event.getEpisode().getDownloadProgress());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(DownladVideoEpisodeErrorEvent errorEvent) {
        int position = Utils.getItemPosition(errorEvent.getEpisode().getId(), episodes);

        if(position != -1) {

            episodes.remove(position);

            adapter.notifyDataSetChanged();
        }

        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void pauseEpisode() {
        mMusicCallbacks.pauseEpisode();
    }

    @Override
    public void playEpisode(AudioEpisode episode) {
        mMusicCallbacks.playEpisode(episode);
    }

    @Override
    public void stopEpisode() {
        mMusicCallbacks.stopEpisode();
    }

    @Override
    public void resumeEpisode() {
        mMusicCallbacks.resumeEpisode();
    }

    @Override
    public void playEpisodeVideo(AudioEpisode episode) {
        mMusicCallbacks.stopEpisode();

        Intent intent = new Intent(OscarApplication.getAppContext(), VideoPlayerActivity.class);

        intent.putExtra(AppConstants.EXTRA_VIDEO_PATH, episode.getPath());

        startActivity(intent);
    }
}
