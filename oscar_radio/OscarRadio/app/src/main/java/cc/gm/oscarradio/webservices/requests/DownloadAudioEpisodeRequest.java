package cc.gm.oscarradio.webservices.requests;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.net.Uri;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.database.AppDBHelper;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.events.DownladAudioEpisodeErrorEvent;
import cc.gm.oscarradio.model.events.EpisodeReceivedEvent;
import cc.gm.oscarradio.utils.Utils;
import de.greenrobot.event.EventBus;

public class DownloadAudioEpisodeRequest extends Thread {
    // constants
    private static final int DOWNLOAD_BUFFER_SIZE = 4096;

    // instance variables
    private Context mContext;
    //private SerialEpisode episode;

    private DownladAudioEpisodeErrorEvent downladErrorEvent;
    private EpisodeReceivedEvent event;

    private AppDBHelper helper;

    /**
     * Instantiates a new DownloaderThread object.
     */
    public DownloadAudioEpisodeRequest(Context context, AudioEpisode episode) {
        downladErrorEvent = new DownladAudioEpisodeErrorEvent();
        event = new EpisodeReceivedEvent();

        mContext = context;

        helper = new AppDBHelper(mContext);

        event.setEpisode(episode);
        downladErrorEvent.setEpisode(episode);

        helper.insertOrUpdateEpisode(event.getEpisode().getId(), event.getEpisode().getParentId()
                , event.getEpisode().getName(), event.getEpisode().getParentName(),
                event.getEpisode().getPath(), event.getEpisode().getVideoLink(), event.getEpisode().getCast());
    }

    /**
     * Connects to the URL of the file, begins the download, and notifies the
     * AndroidFileDownloader activity of changes in state. Writes the file to
     * the root of the SD card.
     */
    @Override
    public void run() {
        URL url;
        URLConnection conn;

        int fileSize;

        BufferedInputStream inStream;
        BufferedOutputStream outStream;

        File outFile;
        FileOutputStream fileStream;

        File outputFile = null;

        try {
            final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

            String urlEncoded = Uri.encode(event.getEpisode().getVideoLink(), ALLOWED_URI_CHARS);

            url = new URL(urlEncoded);


            conn = url.openConnection();
            conn.setUseCaches(false);
            fileSize = conn.getContentLength();

            if (fileSize != -1) {

                if (Utils.freeSpaceAvailable((fileSize / 1024f), Utils.kilobytesAvailable())) {
                    // start download
                    inStream = new BufferedInputStream(conn.getInputStream());

                    String episodePath = event.getEpisode().getParentName();

                    outFile = new File(OscarApplication.getAppContext().getExternalFilesDir(null), episodePath);

                    if (!outFile.exists())
                        outFile.mkdirs(); // failed to create

                    outputFile = new File(outFile, event.getEpisode().getName());

                    fileStream = new FileOutputStream(outputFile);

                    outStream = new BufferedOutputStream(fileStream, DOWNLOAD_BUFFER_SIZE);

                    byte[] data = new byte[DOWNLOAD_BUFFER_SIZE];

                    int bytesRead = 0, totalRead = 0;

                    while (!isInterrupted() && (bytesRead = inStream.read(data, 0, data.length)) >= 0) {
                        outStream.write(data, 0, bytesRead);

                        // update progress bar
                        totalRead += bytesRead;

                        event.getEpisode().setDownloadProgress((int) ((totalRead * 100) / fileSize));
                        event.getEpisode().setDownloadingState(AppConstants.STATE_DOWNLOADING);

                        EventBus.getDefault().post(event);
                    }

                    outStream.close();
                    fileStream.close();
                    inStream.close();

                    if (isInterrupted()) {
                        onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
                    } else {
                        event.getEpisode().setPath(outputFile.getPath());

                        event.getEpisode().setDownloadingState(AppConstants.STATE_DOWNLOADED);

                        EventBus.getDefault().post(event);

                        helper.insertOrUpdateEpisode(event.getEpisode().getId(), event.getEpisode().getParentId()
                                , event.getEpisode().getName(), event.getEpisode().getParentName(),
                                event.getEpisode().getPath(), event.getEpisode().getVideoLink(), event.getEpisode().getCast());

                    }

                } else {
                    onDownloadError(outputFile, mContext.getResources().getString(R.string.not_enough_free_space));
                }

            } else {
                onDownloadError(outputFile, mContext.getResources().getString(R.string.file_not_found));
            }
        } catch (MalformedURLException e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        } catch (FileNotFoundException e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        } catch (Exception e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        }
    }

    private void onDownloadError(File outputFile, String errorMessage) {
        if (outputFile != null)
            outputFile.delete();

        helper.deleteEpisode(downladErrorEvent.getEpisode().getId(), downladErrorEvent.getEpisode().getParentId());

        downladErrorEvent.setErrorMessage(errorMessage);

        downladErrorEvent.getEpisode().setDownloadingState(AppConstants.STATE_NOT_DOWNLOADED);

        EventBus.getDefault().post(downladErrorEvent);
    }

}
