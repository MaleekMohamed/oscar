package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.FavoriteSerialAdapter;
import cc.gm.oscarradio.database.AppDBHelper;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.model.Serial;
import cc.gm.oscarradio.utils.UIUtils;

public class FavoriteSerialsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private View rootView;
    private GridView gvFavSerials;

    private TextView tvEmptyView;

    private ArrayList<Serial> data;
    private FavoriteSerialAdapter adapter;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    private AppDBHelper dbHelper;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        updateFavorites();

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_favorite_serials, container,false);

        initBindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return rootView;
    }

    private void initBindViews() {
        gvFavSerials = (GridView) rootView.findViewById(R.id.gv_fav_serials);

        tvEmptyView = (TextView) rootView.findViewById(R.id.tv_empty_view);

        tvEmptyView.setText(getString(R.string.no_favorite_items));

        gvFavSerials.setOnItemClickListener(this);

        data = new ArrayList<Serial>();

        adapter = new FavoriteSerialAdapter(getActivity(), data);

        gvFavSerials.setAdapter(adapter);

        gvFavSerials.setEmptyView(rootView.findViewById(R.id.tv_empty_view));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        updateFavorites();
    }

    private void updateFavorites() {
        data.clear();

        data.addAll(dbHelper.getSerials());

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        long mId = view.getId();

        Log.d("FukinGridView", "position = " + position);

        if(mId == R.id.iv_adapter_serial_fav) {

            Serial serial = data.get(position);

            showDialog(serial, position);

        } else {

            if(!mActivityCallbacks.getItemClickable(AppConstants.FAVORITE_SERIALS_FRAGMENT_ID)) {
                return;
            }

            mActivityCallbacks.setItemClickable(false, AppConstants.FAVORITE_SERIALS_FRAGMENT_ID);

            SerialDetailsFragment serialsFragment = new SerialDetailsFragment();

            Bundle args = new Bundle();
            args.putString(AppConstants.ARG_SERIAL_ID, data.get(position).getId());
            args.putString(AppConstants.ARG_TITLE, data.get(position).getName());

            serialsFragment.setArguments(args);

            mActivityCallbacks.addFragment(serialsFragment, SerialDetailsFragment.class.getSimpleName());

        }
    }

    private void showDialog(final Serial serial, final int position) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        removeSerialFromFav(serial, position);
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.remove_serial_from_fav_message), serial.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void removeSerialFromFav(Serial serial, int position) {
        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        dbHelper.removeSerialFromFavorites(serial.getId());

        data.remove(position);

        adapter.notifyDataSetChanged();
    }
}
