package cc.gm.oscarradio.model.handlers;

import android.content.Context;
import android.util.Log;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.model.VideoEpisode;

public class VideoEpisodeHandler extends DefaultHandler {

    private final Context mContext;

    private ArrayList<VideoEpisode> videosList;

    private VideoEpisode currentVideoEpisode;

    private String currentItem;

    private String currentId;
    private String currentName;
    private String currentnCast;
    private String currentVideoLink;
    private String currentImage;


    public VideoEpisodeHandler(Context context) {
        super();
        videosList = new ArrayList<VideoEpisode>();
        mContext = context;
    }

    //start of the XML document
    public void startDocument () { Log.i("DataHandler", "Start of XML document");

    }

    //end of the XML document
    public void endDocument () { Log.i("DataHandler", "End of XML document"); }

    //opening element tag
    public void startElement (String uri, String name, String qName, Attributes atts)
    {
        currentItem = qName;

        if(qName.equals(AppConstants.KEY_VIDEO)) {
            currentVideoEpisode = new VideoEpisode();
        }

    }

    //closing element tag
    public void endElement (String uri, String name, String qName)
    {

        if(qName.equals(AppConstants.KEY_VIDEO)) {
            currentVideoEpisode.setId(currentId);
            currentVideoEpisode.setName(currentName);
            currentVideoEpisode.setCast(currentnCast);
            currentVideoEpisode.setVideoLink(currentVideoLink);
            currentVideoEpisode.setImage(currentImage);

            videosList.add(currentVideoEpisode);
        }
        //handle the end of an element
    }

    //element content
    public void characters (char ch[], int start, int length)
    {
        String currText = "";
        //loop through the character array
        for (int i=start; i<start+length; i++)
        {
            switch (ch[i]) {
                case '\\':
                    break;
                case '"':
                    break;
                case '\n':
                    break;
                case '\r':
                    break;
                case '\t':
                    break;
                default:
                    currText += ch[i];
                    break;
            }
        }

        if (currText==null || currText.isEmpty() || currText.length()<=0) return;

        if(currentItem.equals(AppConstants.KEY_VIDEO_ID)){
            currentId =  currText;
        }else if(currentItem.equals(AppConstants.KEY_VIDEO_NAME)){
            currentName =  currText;
        }else if(currentItem.equals(AppConstants.KEY_VIDEO_CAST)){
            currentnCast =  currText;
        }else if(currentItem.equals(AppConstants.KEY_VIDEO_LINK)){
            currentVideoLink =  currText;
        }else if(currentItem.equals(AppConstants.KEY_VIDEO_IMAGE)){
            currentImage =  currText;
        }
        currText="";
    }

    public ArrayList<VideoEpisode> getData(String xml)
    {
        //take care of SAX, input and parsing errors
        try
        {
            //set the parsing driver
            System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");

            //create a parser
            SAXParserFactory parseFactory = SAXParserFactory.newInstance();
            SAXParser xmlParser = parseFactory.newSAXParser();

            //get an XML reader
            XMLReader xmlIn = xmlParser.getXMLReader();

            //instruct the app to use this object as the handler
            xmlIn.setContentHandler(this);

            //Ignore white spaces between elements that causes problem while parsing
            xml = xml.replaceAll(">\\s*<", "><");

            xmlIn.parse(new InputSource(new StringReader(xml)));
        }
        catch(Exception oe) {
            Log.e("AndroidTestsActivity",
                    "Unspecified Error " + oe.getMessage());
        }
        //return the parsed product list
        return videosList;
    }
    public Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

}
