package cc.gm.oscarradio.backgroundtasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.MainActivity;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.events.EpisodePlayingStateEvent;
import cc.gm.oscarradio.utils.UIUtils;
import de.greenrobot.event.EventBus;

public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private static NotificationManager mNotificationManager;

    //media player
    private MediaPlayer player;
    //song list

    private static boolean showNotification = false;
    private int icPlayResourceId = 0;

    private final IBinder musicBind = new MusicBinder();

    private int length = 0;

    private static boolean isRunning = false;

    private static EpisodePlayingStateEvent event;

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        //player.stop();
        //player.release();
        return false;
    }

    @Override
    public void onCreate() {
        //create the service
        super.onCreate();

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        icPlayResourceId = R.drawable.ic_pause_rounded;

        //create player
        player = new MediaPlayer();

        event = new EpisodePlayingStateEvent();

        initMusicPlayer();

        isRunning = true;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getStringExtra(AppConstants.EXTRA_SERVICE_ACTION);

        if(!UIUtils.isEmpty(action)) {
            if(action.equals(AppConstants.ACTION_PLAY_PAUSE)) {

                switch (getEpisode().getPlayingState()) {
                    case AppConstants.STATE_PLAYING:
                        pauseEpisode();
                        break;
                    case AppConstants.STATE_PAUSED:
                        resumeEpisode();
                        break;
                    case AppConstants.STATE_NOT_PLAYING:
                        playEpisode();
                    case AppConstants.STATE_COMPLETED:
                        playEpisode();
                    default:
                        break;
                }

            } else if(action.equals(AppConstants.ACTION_STOP_PLAYING)) {

                stopEpisode();

            } else if(action.equals(AppConstants.ACTION_STOP_MUSIC)) {
                stopEpisode();
                cancelNotification();
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    public void initMusicPlayer(){
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    public void setEpisode(AudioEpisode episode){
        if(player.isPlaying()) {
            event.getEpisode().setPlayingState(AppConstants.STATE_NOT_PLAYING);
            EventBus.getDefault().post(event);
        }

        event.setEpisode(episode);
    }

    public static AudioEpisode getEpisode(){
        return event.getEpisode();
    }

    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    public void playEpisode(){
        icPlayResourceId = R.drawable.ic_pause_rounded;

        if(showNotification) {
            showNotification(getEpisode(), icPlayResourceId);
        }

        event.getEpisode().setPlayingState(AppConstants.STATE_PLAYING);

        player.reset();

        if(event.getEpisode() != null) {
            try{
                player.setDataSource(event.getEpisode().getPath());
            }
            catch(Exception e){
                Log.e("MUSIC SERVICE", "Error setting data source", e);
            }

            player.prepareAsync();
        }
    }

    public void pauseEpisode() {

        icPlayResourceId = R.drawable.ic_play;

        if(showNotification) {
            showNotification(getEpisode(), icPlayResourceId);
        }

        if(player.isPlaying())
        {
            player.pause();

            length = player.getCurrentPosition();

            event.getEpisode().setPlayingState(AppConstants.STATE_PAUSED);

            EventBus.getDefault().post(event);
        }
    }

    public void resumeEpisode() {
        icPlayResourceId = R.drawable.ic_pause_rounded;

        if(showNotification) {
            showNotification(getEpisode(), icPlayResourceId);
        }

        if(player.isPlaying()==false)
        {
            player.seekTo(length);
            player.start();

            event.getEpisode().setPlayingState(AppConstants.STATE_PLAYING);

            EventBus.getDefault().post(event);
        }
    }

    public void stopEpisode() {
        icPlayResourceId = R.drawable.ic_play;

        if(showNotification) {
            showNotification(getEpisode(), icPlayResourceId);
        }

        event.getEpisode().setPlayingState(AppConstants.STATE_NOT_PLAYING);

        EventBus.getDefault().post(event);

        player.stop();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        icPlayResourceId = R.drawable.ic_play;

        if(showNotification) {
            showNotification(getEpisode(), icPlayResourceId);
        }

        event.getEpisode().setPlayingState(AppConstants.STATE_COMPLETED);

        EventBus.getDefault().post(event);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //start playback
        mp.start();

        event.getEpisode().setPlayingState(AppConstants.STATE_PLAYING);

        EventBus.getDefault().post(event);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(showNotification) {
            cancelNotification();
        }

        if (player != null){
            player.stop();
            player.release();
        }

        isRunning = false;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public void showNotification() {
        showNotification = true;
        showNotification(getEpisode(), icPlayResourceId);
    }

    private void showNotification(AudioEpisode episode, int icPlayResourceId){
        if(episode == null) return;

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        //TODO change this to the activity you want to open

        Intent activityToOpen = new Intent().setClass(this, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, activityToOpen, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playIntent = new Intent(AppConstants.ACTION_PLAY_PAUSE);
        PendingIntent playPendingIntent = PendingIntent.getBroadcast(this, 0, playIntent, 0);

        Intent stopPlayingIntent = new Intent(AppConstants.ACTION_STOP_PLAYING);
        PendingIntent stopPlayPendingIntent = PendingIntent.getBroadcast(this, 0, stopPlayingIntent, 0);

        Intent stopMusic = new Intent(AppConstants.ACTION_STOP_MUSIC);
        PendingIntent stopMusicPendingIntent = PendingIntent.getBroadcast(this, 0, stopMusic, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(episode.getParentName())
                        .setContentText(episode.getName())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(episode.getName()))
                        .setPriority(Notification.PRIORITY_MAX)
                        .setShowWhen(false)
                        .setOngoing(true)
                        .addAction(icPlayResourceId, "", playPendingIntent)
                        .addAction(R.drawable.ic_stop, "", stopPlayPendingIntent)
                        .addAction(R.drawable.ic_action_ic_close_rounded, "", stopMusicPendingIntent);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(AppConstants.NOTIFICATION_ID, mBuilder.build());
    }

    public static void cancelNotification(){

        showNotification = false;

        if (Context.NOTIFICATION_SERVICE!=null) {
            String ns = Context.NOTIFICATION_SERVICE;

            if(mNotificationManager == null) {
                mNotificationManager = (NotificationManager)
                        OscarApplication.getAppContext().getSystemService(ns);
            }
            mNotificationManager.cancel(AppConstants.NOTIFICATION_ID);
        }
    }
}