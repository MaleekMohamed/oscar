package cc.gm.oscarradio.model.handlers;

import android.content.Context;
import android.util.Log;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.model.Serial;

public class SerialHandler extends DefaultHandler {

    private final Context mContext;

    private ArrayList<Serial> serialList;

    private Serial currentSerial;

    private String currentItem;

    private String currentId;
    private String currentParentId;
    private String currentName;
    private String currentnCast;
    private String currentSeriesNumber;
    private String currentImage;


    public SerialHandler(Context context) {
        super();
        serialList = new ArrayList<Serial>();
        mContext = context;
    }

    //start of the XML document
    public void startDocument () { Log.i("DataHandler", "Start of XML document");

    }

    //end of the XML document
    public void endDocument () { Log.i("DataHandler", "End of XML document"); }

    //opening element tag
    public void startElement (String uri, String name, String qName, Attributes atts)
    {
        currentItem = qName;

        if(qName.equals(AppConstants.KEY_SERIAL)) {
            currentSerial = new Serial();
        }

    }

    //closing element tag
    public void endElement (String uri, String name, String qName)
    {

        if(qName.equals(AppConstants.KEY_SERIAL)) {
            currentSerial.setId(currentId);
            currentSerial.setParentId(currentParentId);
            currentSerial.setName(currentName);
            currentSerial.setCast(currentnCast);
            currentSerial.setSerialNumber(currentSeriesNumber);
            currentSerial.setImage(currentImage);

            serialList.add(currentSerial);
        }
        //handle the end of an element
    }

    //element content
    public void characters (char ch[], int start, int length)
    {
        String currText = "";
        //loop through the character array
        for (int i=start; i<start+length; i++)
        {
            switch (ch[i]) {
                case '\\':
                    break;
                case '"':
                    break;
                case '\n':
                    break;
                case '\r':
                    break;
                case '\t':
                    break;
                default:
                    currText += ch[i];
                    break;
            }
        }

        if (currText==null || currText.isEmpty() || currText.length()<=0) return;

        if(currentItem.equals(AppConstants.KEY_SERIAL_PARENT_SUB_CAT_ID)){
            currentParentId =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIAL_ID)){
            currentId =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIAL_NAME)){
            currentName =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIALS_NUMBER)){
            currentSeriesNumber =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIAL_CAST)){
            currentnCast =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIAL_IMAGE)){
            currentImage =  currText;
        }
        currText="";
    }

    public ArrayList<Serial> getData(String xml)
    {
        //take care of SAX, input and parsing errors
        try
        {
            //set the parsing driver
            System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");

            //create a parser
            SAXParserFactory parseFactory = SAXParserFactory.newInstance();
            SAXParser xmlParser = parseFactory.newSAXParser();

            //get an XML reader
            XMLReader xmlIn = xmlParser.getXMLReader();

            //instruct the app to use this object as the handler
            xmlIn.setContentHandler(this);

            //Ignore white spaces between elements that causes problem while parsing
            xml = xml.replaceAll(">\\s*<", "><");

            xmlIn.parse(new InputSource(new StringReader(xml)));
        }
        catch(Exception oe) {
            Log.e("AndroidTestsActivity",
                    "Unspecified Error " + oe.getMessage());
        }
        //return the parsed product list
        return serialList;
    }
    public Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

}
