package cc.gm.oscarradio;

import android.os.Environment;

public class AppConstants {
    public static final int SPLASH_INTERVAL = 3000;
    public static final String BASE_URL_DEBUG = "http://globalmedia.cc/Testing/CMS/API/";
    public static final String BASE_URL = "http://www.oscarfm.com/CMS/API/";
    public static final String GET_ALL_CATS_URL = BASE_URL_DEBUG + "Categorieslist.php";
    public static final String GET_SPECIAL_CATS_URL = BASE_URL_DEBUG + "Categorieslist.php";

    public static final String GET_SUB_CATS_URL = BASE_URL_DEBUG + "SubCatList.php?CatID=";
    public static final String GET_SERIALS_URL = BASE_URL_DEBUG + "SerialList.php?Paging=1&Sorting=1&SubCatID=";
    public static final String GET_MOST_DOWNLOADED_SERIALS_URL = BASE_URL_DEBUG + "Most_Download.php?Paging=1&Sorting=1";
    public static final String GET_MOST_WATCHED_SERIALS_URL = BASE_URL_DEBUG + "Most_Watch.php?Paging=1&Sorting=1";
    public static final String GET_MOST_LIKED_SERIALS_URL = BASE_URL_DEBUG + "Most_Like.php?Paging=1&Sorting=1";
    public static final String GET_SERIAL_DETAILS_URL = BASE_URL_DEBUG + "SerialDetails.php?SerialID=";
    public static final String GET_SPECIAL_SERIAL_DETAILS_URL = BASE_URL_DEBUG + "VideoList.php?Paging=1&Sorting=1";
    public static final String GET_COMING_SOON_URL = BASE_URL_DEBUG + "VideoList.php?Paging=1&Sorting=1";
    public static final String SEARCH_URL = BASE_URL_DEBUG + "SearchSerial.php?Keyword=";

    public static final String DO_DOWNLOAD_URL = BASE_URL_DEBUG + "Do_Download.php?City=Cairo&Country=Egypt&Date=";
    public static final String DO_LIKE_URL = BASE_URL_DEBUG + "Do_Like.php?City=Cairo&Country=Egypt&Date=";
    public static final String DO_WATCH_URL = BASE_URL_DEBUG + "Do_Watch.php?City=Cairo&Country=Egypt&Date=";

    public static final String ARG_CAT_ID = "MainCatId";
    public static final String ARG_SUB_CAT_ID = "supCatId";
    public static final String ARG_TITLE = "actionBarTitle";
    public static final String ARG_MOST_TYPE = "mostType";
    public static final String ARG_SERIAL_ID = "serialId";
    public static final String ARG_SEARCH_KEYWORK = "searchKeyword";

    public static final int CAT_FRAGMENT_ID = 0;
    public static final int SUB_CAT_FRAGMENT_ID = 1;
    public static final int SERIALS_FRAGMENT_ID = 2;
    public static final int FAVORITE_SERIALS_FRAGMENT_ID = 3;
    public static final int SEARCH_FRAGMENT_ID = 5;
    public static final int SPECIAL_SERIALS_FRAGMENT_ID = 6;

    public static final int MOST_WATCHED_SERIALS = 0;
    public static final int MOST_LIKED_SERIALS = 1;
    public static final int MOST_DOENLOADED_SERIALS = 2;
    public static final int SEARCH_SERIALS = 3;

    public static final String KEY_CATEGORIES = "Categories";
    public static final String KEY_CAT_ID = "Id";
    public static final String KEY_CAT_NAME = "CategroyName";
    public static final String KEY_CAT_ICON = "CategroyIcon";
    public static final String KEY_CAT_ACTIVE = "Active";
    public static final String KEY_SUB_CATEGORY = "Subcategory";
    public static final String KEY_SUB_CAT_PARENT_CAT_ID = "CatId";
    public static final String KEY_SUB_CAT_ID = "SubCatId";
    public static final String KEY_SUB_CAT_NAME = "SubcategroyName";
    public static final String KEY_SUB_CAT_SERIALS_NUMBER = "SerialsNumber";
    public static final String KEY_SUB_CAT_NEW_ITEMS = "NewItems";
    public static final String KEY_SUB_CAT_IMAGE = "Image";
    public static final String KEY_SERIAL = "Serial";
    public static final String KEY_SERIAL_PARENT_SUB_CAT_ID = "SubCatID";
    public static final String KEY_SERIAL_ID = "SeriaId";
    public static final String KEY_SERIAL_NAME = "SerialName";
    public static final String KEY_SERIAL_CAST = "SerialCast";
    public static final String KEY_SERIALS_NUMBER = "SeriesNumber";
    public static final String KEY_SERIAL_IMAGE = "Image";
    public static final String KEY_SERIAL_DOWNLOADES = "SerialDownloads";
    public static final String KEY_SERIAL_LIKES = "SerialLikes";
    public static final String KEY_SERIAL_DIRECTOR = "Director";
    public static final String KEY_SERIAL_WRITER = "Writer";
    public static final String KEY_SERIAL_BRIEF = "Brief";
    public static final String KEY_SERIALS_LENGHT = "Serieslenght";
    public static final String KEY_EPISODES_LIST = "SeriesList";
    public static final String KEY_EPISODE = "Series";
    public static final String KEY_EPISODE_PARENT_SERIAL_ID = "SerialID";
    public static final String KEY_EPISODE_ID = "SeriesId";
    public static final String KEY_EPISODE_NAME = "SeriesName";
    public static final String KEY_EPISODE_DOWNLOADS = "Downloads";
    public static final String KEY_EPISODE_WATCH = "Watch";
    public static final String KEY_EPISODE_IMAGE = "Image";
    public static final String KEY_EPISODE_VIDEO_LINK = "videolink";
    public static final String KEY_VIDEO = "Video";
    public static final String KEY_VIDEO_ID = "VideoId";
    public static final String KEY_VIDEO_NAME = "VideoName";
    public static final String KEY_VIDEO_CAST = "VideoCast";
    public static final String KEY_VIDEO_LINK = "VideoLink";
    public static final String KEY_VIDEO_IMAGE = "Image";

    //public static final String OSCAR_DIR_ON_SDCARD = Environment.getExternalStorageDirectory() + "/oscar/";

    public static final int STATE_NOT_PLAYING = 0;
    public static final int STATE_PLAYING = 1;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_COMPLETED = 3;

    public static final int STATE_DOWNLOADING = 0;
    public static final int STATE_DOWNLOADED = 1;
    public static final int STATE_NOT_DOWNLOADED = 2;

    public static final String PARAMETER_EPISODE_ID = "&SerialID=";

    public static final int SOCKET_TIMEOUT = 30000;

    public static final String KEY_STATUS = "Status";

    public static final String REQUEST_CATEGORIES_TAG = "requestCategoriesTag";
    public static final String REQUEST_SUB_CATEGORIES_TAG = "requestSubCategoriesTag";
    public static final String REQUEST_SERIALS_TAG = "requestSerialsTag";
    public static final String REQUEST_SEARCH_TAG = "requestSearchTag";
    public static final String REQUEST_SERIAL_DETAILS_TAG = "requestSerialDetailsTag";
    public static final String REQUEST_MOST_DOWNLOADS_TAG = "requestMostDownloadsTag";
    public static final String REQUEST_MOST_WATCHES_TAG = "requestMostWatchesTag";
    public static final String REQUEST_MOST_LIKES_TAG = "requestMostLikesTag";
    public static final String REQUEST_DO_DOENLOADED_TAG = "requestDoDownloadedTag";
    public static final String REQUEST_DO_LIKED_TAG = "requestDoLikedTag";
    public static final String REQUEST_DO_WATCHED_TAG = "requestDoWatchedTag";
    public static final String REQUEST_COMING_SOON_TAG = "requestComingSoonTsg";

    public static final boolean OPEN_DOWNLOADS_ON_CLICK = true;

    public static final String SPECIAL_CATEGORY_ID = "1000";
    public static final String COMING_SOON_CATEGORY_ID = "-";
    public static final String STATE_ACTIVE = "1";

    public static final String EXTRA_VIDEO_PATH = "videoPath";
    public static final String EXTRA_VIDEO_CURRENT_POSITION = "currentPosition";
    public static final String COMING_SOON_CATEGORY_NAME = "يعرض قريبا";

    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_POSITIVE_BUTTON_STR = "yesStr";
    public static final String KEY_NEGATIVE_BUTTON_STR = "noStr";

    public static final String ACTION_PLAY_PAUSE = "cc.gm.oscarradio.ACTION_PLAY_PAUSE";
    public static final String ACTION_STOP_PLAYING = "cc.gm.oscarradio.ACTION_STOP_PLAYING";
    public static final String ACTION_STOP_MUSIC = "cc.gm.oscarradio.ACTION_STOP_MUSIC";
    public static final String EXTRA_SERVICE_ACTION = "extraServiceAction";
    public static final int NOTIFICATION_ID = 112233;
}
