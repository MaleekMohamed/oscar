package cc.gm.oscarradio.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;

import java.io.File;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.VideoEpisode;

public class Utils {

    public static boolean episodeExists(String path) {
        File file = new File(path);

        return file.exists();
    }

    public static String getEpisodePath(AudioEpisode item) {
        return OscarApplication.getAppContext().getExternalFilesDir(null).getPath() + "/"
                + item.getParentName() + "/" + item.getName();
    }

    public static String getEpisodePath(VideoEpisode item) {
        return OscarApplication.getAppContext().getExternalFilesDir(null).getPath() + "/"
                + item.getParentName() + "/" + item.getName();
    }

    public static int getItemPosition(AudioEpisode episode, ArrayList<AudioEpisode> episodes) {
        for (int i = 0; i < episodes.size(); i++) {
            if (episode.getParentId().equalsIgnoreCase(episodes.get(i).getParentId()) &&
                    episode.getId().equalsIgnoreCase(episodes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public static int getItemPosition(VideoEpisode episode, ArrayList<VideoEpisode> episodes) {
        for (int i = 0; i < episodes.size(); i++) {
            if (episode.getId().equalsIgnoreCase(episodes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public static int getItemPosition(String id, ArrayList<AudioEpisode> episodes) {
        for (int i = 0; i < episodes.size(); i++) {
            if (id.equalsIgnoreCase(episodes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isAudio(String path) {
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(path,
                MediaStore.Images.Thumbnails.MINI_KIND);

        return thumbnail == null;
    }

    public static float kilobytesAvailable() {
        if (Environment.getExternalStorageDirectory() != null
                && Environment.getExternalStorageDirectory().getPath() != null) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());

            long bytesAvailable = 0;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                bytesAvailable = (long) stat.getBlockSizeLong() * (long) stat.getAvailableBlocksLong();
            } else {
                bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
            }

            return bytesAvailable / (1024.f);
        }

        return -1;
    }

    public static boolean freeSpaceAvailable(float fileSizeInKilobytes, float kilobytesAvailable) {
        return kilobytesAvailable > fileSizeInKilobytes;
    }

}
