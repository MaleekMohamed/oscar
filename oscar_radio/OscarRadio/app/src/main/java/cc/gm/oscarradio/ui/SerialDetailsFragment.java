package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.SerialEpisodeAdapter;
import cc.gm.oscarradio.backgroundtasks.MusicService;
import cc.gm.oscarradio.database.AppDBHelper;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.interfaces.DownloadCallbacksInterface;
import cc.gm.oscarradio.interfaces.MusicCallbacksInterface;
import cc.gm.oscarradio.model.events.DownladAudioEpisodeErrorEvent;
import cc.gm.oscarradio.model.events.EpisodePlayingStateEvent;
import cc.gm.oscarradio.model.events.EpisodeReceivedEvent;
import cc.gm.oscarradio.model.Serial;
import cc.gm.oscarradio.model.SerialDetails;
import cc.gm.oscarradio.model.events.SerialDetailsErrorReceivedEvent;
import cc.gm.oscarradio.model.events.SerialDetailsReceivedEvent;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.utils.Utils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.GetSerialDetailsRequest;
import de.greenrobot.event.EventBus;

public class SerialDetailsFragment extends ProgressFragment implements View.OnClickListener,
        SerialEpisodeAdapter.AdapterCallback {

    private SerialDetails serialDetails;
    private Serial serial = null;

    private ArrayList<AudioEpisode> episodes;

    private SerialEpisodeAdapter adapter;

    private View rootView;
    private LinearLayout llSerialCast;
    private ImageView ivSerialCast, ivFavorite;
    private ImageView ivSerialImage;
    private TextView tvSerialName, tvSerialBrief, tvNumberOfEpisodes, tvEpisodeDuration,
            tvNumberOfDownloads, tvNumberOfLikes, tvSerialCast, tvSerialDirector, tvSerialWriter;
    private ListView lvEpisodes;

    private String serialId;
    private String title;

    private AppDBHelper dbHelper;

    private ActivityCallbacksInterface mActivityCallbacks;
    private MusicCallbacksInterface mMusicCallbacks;
    private DownloadCallbacksInterface mDownloadCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
            mMusicCallbacks = (MusicCallbacksInterface) activity;
            mDownloadCallbacks = (DownloadCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        if(dbHelper != null && serialDetails != null) {
            serial = dbHelper.getSerialById(serialDetails.getParentId());

            if(serial == null) {
                if(ivFavorite != null)
                    ivFavorite.setImageResource(R.drawable.favourites);
            } else {
                if(ivFavorite != null)
                    ivFavorite.setImageResource(R.drawable.favourited);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.serialId = getArguments().getString(AppConstants.ARG_SERIAL_ID);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_serial_details, container,false);

        initBindViews();

        //setupCustomActionBar(title);

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return rootView;
    }

    private void initBindViews() {
        ivFavorite = (ImageView) rootView.findViewById(R.id.iv_serial_details_fav);

        ivSerialImage = (ImageView) rootView.findViewById(R.id.iv_serial_details_icon);

        tvSerialName = (TextView) rootView.findViewById(R.id.tv_serial_details_name);
        tvSerialBrief = (TextView) rootView.findViewById(R.id.tv_serial_details_brief);

        tvNumberOfEpisodes = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_episodes);

        tvEpisodeDuration = (TextView) rootView.findViewById(R.id.tv_serial_details_episode_duration);

        tvNumberOfDownloads = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_downloads);
        tvNumberOfLikes = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_likes);

        llSerialCast = (LinearLayout) rootView.findViewById(R.id.ll_serial_details_cast);

        ivSerialCast = (ImageView) rootView.findViewById(R.id.iv_serial_details_cast_drop_down);
        tvSerialCast = (TextView) rootView.findViewById(R.id.tv_serial_details_cast);

        tvSerialDirector = (TextView) rootView.findViewById(R.id.tv_serial_details_director);

        tvSerialWriter = (TextView) rootView.findViewById(R.id.tv_serial_details_writer);

        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_serial_details_episodes);

        // Set buttons effect
        UIUtils.setImageClickEffect(ivFavorite);

        // Init click Listeners
        llSerialCast.setOnClickListener(this);
        ivFavorite.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        episodes = new ArrayList<AudioEpisode>();

        adapter = new SerialEpisodeAdapter(OscarApplication.getAppContext(), episodes, this);

        lvEpisodes.setAdapter(adapter);

        setContentShown(false);

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSerialDetails(serialId);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SERIALS_FRAGMENT_ID);
        mActivityCallbacks.setItemClickable(true, AppConstants.FAVORITE_SERIALS_FRAGMENT_ID);
        mActivityCallbacks.setItemClickable(true, AppConstants.SEARCH_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(SerialDetailsReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SerialDetailsErrorReceivedEvent.class);

        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest()
                .cancelAllRequestes(AppConstants.REQUEST_SERIAL_DETAILS_TAG);
    }

    // This method will be called when a MessageEvent is posted
    public void onEventMainThread(SerialDetailsReceivedEvent event){

        mActivityCallbacks.disableRefresh();

        if(ivFavorite != null) {
            ivFavorite.setEnabled(true);
        }

        serialDetails = event.getSerialDetails();

        episodes.clear();
        episodes.addAll(serialDetails.getAudioEpisodes());

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            }
        }

        if(MusicService.isRunning()) {
            AudioEpisode episode = MusicService.getEpisode();
            if(episode != null) {
                int position = Utils.getItemPosition(episode, episodes);

                if(position != -1) {
                    episodes.get(position).setPlayingState(episode.getPlayingState());
                }
            }
        }

        adapter.notifyDataSetChanged();

        updateUI(serialDetails);

        setContentShown(true);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SerialDetailsErrorReceivedEvent event){
        GetSerialDetailsRequest request = new GetSerialDetailsRequest(serialId);

        mActivityCallbacks.enableRefresh(request, AppConstants.REQUEST_SERIAL_DETAILS_TAG);

        if(ivFavorite != null) {
            ivFavorite.setEnabled(false);
        }

        String errorMessage = event.getErrorMessage();

        Toast.makeText(OscarApplication.getAppContext(), errorMessage, Toast.LENGTH_LONG).show();

        setContentShown(true);
    }

    private void updateUI(SerialDetails serialDetails) {
        tvSerialName.setText(serialDetails.getName());
        tvSerialBrief.setText(serialDetails.getBrief());

        Picasso.with(OscarApplication.getAppContext())
                .load(serialDetails.getImage())
                .error(R.drawable.default_image)
                .into(ivSerialImage);

        tvNumberOfEpisodes.setText(serialDetails.getSeriesNumber());
        tvEpisodeDuration.setText(serialDetails.getSerieslenght());
        tvSerialCast.setText(serialDetails.getCast());
        tvSerialDirector.setText(serialDetails.getDirector());
        tvSerialWriter.setText(serialDetails.getWriter());
        tvNumberOfDownloads.setText(serialDetails.getDownloads());
        tvNumberOfLikes.setText(serialDetails.getLikes());

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        serial = dbHelper.getSerialById(serialDetails.getParentId());

        if(serial == null) {
            ivFavorite.setImageResource(R.drawable.favourites);
        } else {
            ivFavorite.setImageResource(R.drawable.favourited);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_serial_details_cast:
                doDropDown();
                break;
            case R.id.iv_action_bar_back:
                mActivityCallbacks.doBack();
                break;
            case R.id.iv_serial_details_fav:
                if(serialDetails == null) return;
                addToFavorite();
                break;
            default:
                break;
        }
    }

    private void doDropDown() {
        if(tvSerialCast.isShown()) {
            ivSerialCast.setImageResource(R.drawable.drop_down_right);
            tvSerialCast.setVisibility(View.GONE);
        } else {
            ivSerialCast.setImageResource(R.drawable.drop_down);
            tvSerialCast.setVisibility(View.VISIBLE);
        }
    }

    private void addToFavorite() {
        if(serial == null) {
            serial = new Serial();

            serial.setId(serialDetails.getParentId());
            serial.setName(serialDetails.getName());
            serial.setCast(serialDetails.getCast());
            serial.setImage(serialDetails.getImage());

            dbHelper.insertSerial(serial);

            ivFavorite.setImageResource(R.drawable.favourited);

            RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().doLiked(serialDetails.getParentId());
        } else {
            showDialog();
        }
    }

    private void showDialog() {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        removeSerialFromFav();
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.remove_serial_from_fav_message), serial.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void removeSerialFromFav() {
        boolean deleted = dbHelper.removeSerialFromFavorites(serial.getId());

        ivFavorite.setImageResource(R.drawable.favourites);

        serial = null;
    }

    public void onEventMainThread(EpisodePlayingStateEvent event) {
        int position = Utils.getItemPosition(event.getEpisode(), episodes);

        if(position != -1) {
            episodes.get(position).setPlayingState(event.getEpisode().getPlayingState());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(EpisodeReceivedEvent event) {
        int position = Utils.getItemPosition(event.getEpisode(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(event.getEpisode().getPath());
            episodes.get(position).setDownloadingState(event.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(event.getEpisode().getDownloadProgress());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(DownladAudioEpisodeErrorEvent errorEvent) {
        int position = Utils.getItemPosition(errorEvent.getEpisode(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(errorEvent.getEpisode().getPath());
            episodes.get(position).setDownloadingState(errorEvent.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(0);

            adapter.notifyDataSetChanged();
        }

        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void pauseEpisode() {
        mMusicCallbacks.pauseEpisode();
    }

    @Override
    public void playEpisode(AudioEpisode episode) {
        mMusicCallbacks.playEpisode(episode);
    }

    @Override
    public void stopEpisode() {
        mMusicCallbacks.stopEpisode();
    }

    @Override
    public void resumeEpisode() {
        mMusicCallbacks.resumeEpisode();
    }

    @Override
    public void openDownloadedEpisodes() {
        mDownloadCallbacks.openDownloadedEpisodes();
    }

    @Override
    public void playEpisodeVideo(AudioEpisode episode) {
        mMusicCallbacks.stopEpisode();

        Intent intent = new Intent(OscarApplication.getAppContext(), VideoPlayerActivity.class);

        intent.putExtra(AppConstants.EXTRA_VIDEO_PATH, episode.getPath());

        startActivity(intent);
    }
}

