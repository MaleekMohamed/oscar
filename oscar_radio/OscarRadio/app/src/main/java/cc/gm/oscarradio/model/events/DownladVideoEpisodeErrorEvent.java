package cc.gm.oscarradio.model.events;

import cc.gm.oscarradio.model.VideoEpisode;

public class DownladVideoEpisodeErrorEvent {

    private String errorMessage;
    private VideoEpisode episode;

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public VideoEpisode getEpisode() {
        return episode;
    }

    public void setEpisode(VideoEpisode episode) {
        this.episode = episode;
    }

}
