package cc.gm.oscarradio.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.filippudak.ProgressPieView.ProgressPieView;

import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.utils.NetworkManager;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.utils.Utils;
import cc.gm.oscarradio.webservices.requests.DownloadAudioEpisodeRequest;
import cc.gm.oscarradio.webservices.requests.DownloadedRequest;

public class DownloadedEpisodeAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<AudioEpisode> audioEpisodes;

    private DownloadAudioEpisodeRequest episodeTask;

    private AdapterCallback callback;

    private DownloadedRequest downloadedRequest;

    public interface AdapterCallback {
        public void pauseEpisode();
        public void playEpisode(AudioEpisode episode);
        public void stopEpisode();
        public void resumeEpisode();
        public void playEpisodeVideo(AudioEpisode episode);
    }

    public DownloadedEpisodeAdapter(Context context, ArrayList<AudioEpisode> audioEpisodes, Fragment fragment) {
        this.mContext = context;
        this.audioEpisodes = audioEpisodes;
        try {
            this.callback = ((AdapterCallback) fragment);
        } catch (ClassCastException e) {
            throw new ClassCastException("Fragment must implement AdapterCallback.");
        }
    }

    @Override
    public int getCount() {
        return audioEpisodes.size();
    }

    @Override
    public Object getItem(int position) {
        return audioEpisodes.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(audioEpisodes.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final AudioEpisode item = audioEpisodes.get(position);

        ItemViewHolder itemViewHolder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_downloaded_episode, null);

            itemViewHolder = new ItemViewHolder();

            itemViewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_adapter_downloaded_episode_name);
            itemViewHolder.rlPlayStop = (RelativeLayout) convertView.findViewById(R.id.rl_adapter_download_play);
            itemViewHolder.ivPlayStop = (ImageView) convertView.findViewById(R.id.iv_adapter_downloaded_play);
            itemViewHolder.pvDownloadBar = (ProgressPieView) convertView.findViewById(R.id.pv_adapter_download);
            itemViewHolder.tvParentName = (TextView) convertView.findViewById(R.id.tv_adapter_downloaded_serial_name);

            convertView.setTag(itemViewHolder);
        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }


        if(itemViewHolder != null) {


            switch (item.getDownloadingState()) {
                case AppConstants.STATE_DOWNLOADING:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setText(item.getDownloadProgress() + "%");

                        itemViewHolder.pvDownloadBar.setProgress(item.getDownloadProgress());

                        itemViewHolder.pvDownloadBar.setVisibility(View.VISIBLE);

                        if(itemViewHolder.ivPlayStop != null) {
                            itemViewHolder.ivPlayStop.setImageResource(R.drawable.ic_play);
                            itemViewHolder.ivPlayStop.setVisibility(View.GONE);
                        }

                    }
                    break;

                case AppConstants.STATE_NOT_DOWNLOADED:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setVisibility(View.GONE);

                        if(itemViewHolder.ivPlayStop != null) {
                            itemViewHolder.ivPlayStop.setImageResource(R.drawable.ic_download);
                            itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);
                        }
                    }

                    break;

                case AppConstants.STATE_DOWNLOADED:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setVisibility(View.GONE);

                        if(itemViewHolder.ivPlayStop != null) {
                            if(item.getPlayingState() == AppConstants.STATE_PLAYING) {
                                itemViewHolder.ivPlayStop.setImageResource(R.drawable.ic_pause);
                                itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);
                            } else {
                                itemViewHolder.ivPlayStop.setImageResource(R.drawable.ic_play);
                                itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);
                            }

                        }
                    }

                    break;

                default:
                    break;
            }

            if(itemViewHolder.rlPlayStop != null) {
                itemViewHolder.rlPlayStop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageView ivPlayStop = (ImageView) v.findViewById(R.id.iv_adapter_downloaded_play);
                        ProgressPieView pvDownloadBar = (ProgressPieView) v.findViewById(R.id.pv_adapter_download);

                        switch (item.getDownloadingState()) {
                            case AppConstants.STATE_DOWNLOADING:

//                                pvDownloadBar.setText(item.getDownloadProgress() + "%");
//
//                                pvDownloadBar.setProgress(item.getDownloadProgress());
//
//                                pvDownloadBar.setVisibility(View.VISIBLE);
//
//                                ivPlayStop.setImageResource(R.drawable.ic_play);
//                                ivPlayStop.setVisibility(View.GONE);

                                break;

                            case AppConstants.STATE_NOT_DOWNLOADED:

                                if(NetworkManager.isInternetConnected()) {
                                    episodeTask = new DownloadAudioEpisodeRequest(mContext, item);

                                    episodeTask.start();

                                    pvDownloadBar.setVisibility(View.VISIBLE);

                                    ivPlayStop.setImageResource(R.drawable.ic_play);
                                    ivPlayStop.setVisibility(View.GONE);
                                } else {
                                    Toast.makeText(mContext, mContext.getResources().getString(
                                            R.string.no_connection), Toast.LENGTH_SHORT).show();
                                }
                                break;

                            case AppConstants.STATE_DOWNLOADED:

                                if(Utils.isAudio(item.getPath())) {
                                    switch (item.getPlayingState()) {
                                        case AppConstants.STATE_PLAYING:

                                            item.setPlayingState(AppConstants.STATE_PAUSED);

                                            ivPlayStop.setImageResource(R.drawable.ic_play);
                                            callback.pauseEpisode();

                                            break;
                                        case AppConstants.STATE_NOT_PLAYING:

                                            item.setPlayingState(AppConstants.STATE_PLAYING);

                                            ivPlayStop.setImageResource(R.drawable.ic_pause);
                                            callback.playEpisode(item);

                                            break;
                                        case AppConstants.STATE_PAUSED:

                                            item.setPlayingState(AppConstants.STATE_PLAYING);

                                            ivPlayStop.setImageResource(R.drawable.ic_pause);
                                            callback.resumeEpisode();

                                            break;
                                        case AppConstants.STATE_COMPLETED:

                                            item.setPlayingState(AppConstants.STATE_NOT_PLAYING);

                                            ivPlayStop.setImageResource(R.drawable.ic_pause);
                                            callback.playEpisode(item);

                                            break;
                                        default:
                                            break;
                                    }
                                } else {
                                    callback.playEpisodeVideo(item);
                                }

                                break;

                            default:
                                break;
                        }
                    }
                });
            }

            if(itemViewHolder.tvName != null)
                itemViewHolder.tvName.setText(item.getName());

            if(itemViewHolder.tvParentName != null)
                itemViewHolder.tvParentName.setText(item.getParentName());
        }

        UIUtils.overrideFonts(mContext, convertView);

        return convertView;
    }

    static class ItemViewHolder {
        public TextView tvName;
        public RelativeLayout rlPlayStop;
        public ImageView ivPlayStop;
        public ProgressPieView pvDownloadBar;
        public TextView tvParentName;
    }
}