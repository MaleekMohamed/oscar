package cc.gm.oscarradio.model;

import java.util.ArrayList;

public class SerialDetails {
    private String id;
    private String parentId;
    private String name;
    private String cast;
    private String seriesNumber;
    private String director;
    private String writer;
    private String brief;
    private String serieslenght;
    private String image;
    private String downloads;
    private String likes;

    private ArrayList<AudioEpisode> audioEpisodes;

    public SerialDetails(){

    }

    @Override
    public String toString() {
        return "MainCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public SerialDetails(String id, String seriesName, String cast, String seriesNumber,
                         String director, String writer, String brief, String serieslenght, String image,
                         String downloads, String likes) {
        this.id = id;
        this.name = seriesName;
        this.cast = cast;
        this.seriesNumber = seriesNumber;
        this.director = director;
        this.writer = writer;
        this.brief = brief;
        this.serieslenght = serieslenght;
        this.image = image;
        this.downloads = downloads;
        this.likes = likes;
    }

    public SerialDetails(String id, String seriesName, String cast, String seriesNumber,
                         String director, String writer, String brief, String serieslenght,
                         String image, ArrayList<AudioEpisode> audioEpisodes) {
        this.id = id;
        this.name = seriesName;
        this.cast = cast;
        this.seriesNumber = seriesNumber;
        this.director = director;
        this.writer = writer;
        this.brief = brief;
        this.serieslenght = serieslenght;
        this.image = image;
        this.audioEpisodes = audioEpisodes;
    }

    public String getId() {
        return id;
    }

    public String getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getCast() {
        return cast;
    }

    public String getSeriesNumber() {
        return seriesNumber;
    }

    public String getImage() {
        return image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setSeriesNumber(String seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getSerieslenght() {
        return serieslenght;
    }

    public void setSerieslenght(String serieslenght) {
        this.serieslenght = serieslenght;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public ArrayList<AudioEpisode> getAudioEpisodes() {
        return audioEpisodes;
    }

    public void setAudioEpisodes(ArrayList<AudioEpisode> audioEpisodes) {
        this.audioEpisodes = audioEpisodes;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
