package cc.gm.oscarradio.model.events;

import cc.gm.oscarradio.model.AudioEpisode;

public class DownladAudioEpisodeErrorEvent {

    private String errorMessage;
    private AudioEpisode episode;

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public AudioEpisode getEpisode() {
        return episode;
    }

    public void setEpisode(AudioEpisode episode) {
        this.episode = episode;
    }
}
