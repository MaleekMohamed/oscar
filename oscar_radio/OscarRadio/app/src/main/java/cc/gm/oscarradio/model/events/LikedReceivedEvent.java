package cc.gm.oscarradio.model.events;

public class LikedReceivedEvent {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
