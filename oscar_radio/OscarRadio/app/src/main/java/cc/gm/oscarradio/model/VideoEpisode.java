package cc.gm.oscarradio.model;

import cc.gm.oscarradio.AppConstants;

public class VideoEpisode {

    private String parentId;
    private String parentName;
    private String id;
    private String name;
    private String cast;
    private String videoLink;
    private String image;
    private String path;
    private int playingState;
    private int downloadingState;
    private int downloadProgress;

    public VideoEpisode() {
        this.parentId = AppConstants.COMING_SOON_CATEGORY_ID;
        this.parentName = AppConstants.COMING_SOON_CATEGORY_NAME;
        this.playingState = AppConstants.STATE_NOT_PLAYING;
        this.downloadingState = AppConstants.STATE_NOT_DOWNLOADED;
        this.downloadProgress = 0;
    }

    public VideoEpisode(String id, String name, String cast, String videoLink, String image, String path) {
        this.parentId = AppConstants.COMING_SOON_CATEGORY_ID;
        this.parentName = AppConstants.COMING_SOON_CATEGORY_NAME;
        this.id = id;
        this.name = name;
        this.cast = cast;
        this.videoLink = videoLink;
        this.image = image;
        this.path = path;
        this.playingState = AppConstants.STATE_NOT_PLAYING;
        this.downloadingState = AppConstants.STATE_NOT_DOWNLOADED;
        this.downloadProgress = 0;
    }

    public VideoEpisode(String id, String name, String cast, String videoLink, String image,
                        String path, int playingState, int downloadingState, int downloadProgress) {
        this.parentId = AppConstants.COMING_SOON_CATEGORY_ID;
        this.parentName = AppConstants.COMING_SOON_CATEGORY_NAME;
        this.id = id;
        this.name = name;
        this.cast = cast;
        this.videoLink = videoLink;
        this.image = image;
        this.path = path;
        this.playingState = playingState;
        this.downloadingState = downloadingState;
        this.downloadProgress = downloadProgress;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPlayingState() {
        return playingState;
    }

    public void setPlayingState(int playingState) {
        this.playingState = playingState;
    }

    public int getDownloadingState() {
        return downloadingState;
    }

    public void setDownloadingState(int downloadingState) {
        this.downloadingState = downloadingState;
    }

    public int getDownloadProgress() {
        return downloadProgress;
    }

    public void setDownloadProgress(int downloadProgress) {
        this.downloadProgress = downloadProgress;
    }
}
