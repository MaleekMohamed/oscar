
package cc.gm.oscarradio.ui;

import java.io.IOException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.R;


public class VideoPlayerActivity extends Activity
{
	private VideoView myVideoView;
	private int currentPos = -1;
	private String videoPath;
	private SecretKey secretKey;
	private IvParameterSpec paramSpec;
	private static byte[] AlgorithmParams = { (byte) 0xB2, (byte) 0x12, (byte) 0xD5,
		(byte) 0xB2, (byte) 0x44, (byte) 0x21, (byte) 0xC3, (byte) 0xC3,
		(byte) 0xB2, (byte) 0x12, (byte) 0xD5, (byte) 0xB2, (byte) 0x44,
		(byte) 0x21, (byte) 0xC3, (byte) 0xC3 };

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_player);

		Intent i = getIntent();

		videoPath = i.getStringExtra(AppConstants.EXTRA_VIDEO_PATH);

		if(videoPath!=null && videoPath.length()>0)
		{
			try {
				myVideoView= (VideoView) findViewById(R.id.videoView1);

				myVideoView.setVideoPath(videoPath);
				myVideoView.setMediaController(new MediaController(this));
				myVideoView.requestFocus();

				if (savedInstanceState != null) {
					currentPos = savedInstanceState.getInt(AppConstants.EXTRA_VIDEO_CURRENT_POSITION,-1);
					myVideoView.pause();

				}

				startPlayingVideo(currentPos-5000 , videoPath);


			} catch (Exception e) {
				e.printStackTrace();
				//startActivity(new Intent(getApplicationContext(), DashBoard.class));
				finish();
			}



		}

	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (myVideoView !=null && myVideoView.isPlaying()) {
		
			outState.putInt(AppConstants.EXTRA_VIDEO_CURRENT_POSITION, myVideoView.getCurrentPosition());
			
			myVideoView.pause();
		}

		
	}
	
	public void startPlayingVideo(int seekPosition, String videoPath) throws IOException
	{

		myVideoView.setVideoPath(videoPath);

		if (seekPosition != -1) {
			myVideoView.pause();
			myVideoView.seekTo(seekPosition);		
		}

		myVideoView.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {

			}
		});
	
		myVideoView.start();

	}
}