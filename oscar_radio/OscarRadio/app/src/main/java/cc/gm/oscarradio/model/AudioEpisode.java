package cc.gm.oscarradio.model;

import cc.gm.oscarradio.AppConstants;

public class AudioEpisode {

    private String parentId;
    private String parentName;
    private String id;
    private String name;
    private String downloads;
    private String watch;
    private String image;
    private String videoLink;
    private String path;
    private String cast;
    private int playingState;
    private int downloadingState;
    private int downloadProgress;

    @Override
    public String toString() {
        return "MainCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public AudioEpisode() {
        this.playingState = AppConstants.STATE_NOT_PLAYING;
        this.downloadingState = AppConstants.STATE_NOT_DOWNLOADED;
        this.downloadProgress = 0;
    }

    public AudioEpisode(String id, String name, String downloads, String watch,
                        String image, String videoLink, int watching, String path, int downloadingState, int playingState, int downloadProgress) {
        this.id = id;
        this.name = name;
        this.downloads = downloads;
        this.watch = watch;
        this.image = image;
        this.videoLink = videoLink;
        this.path = path;
        this.playingState = playingState;
        this.downloadingState = downloadingState;
        this.downloadProgress = downloadProgress;
    }

    public AudioEpisode(String id, String name, String downloads, String watch,
                        String image, String videoLink) {
        this.id = id;
        this.name = name;
        this.downloads = downloads;
        this.watch = watch;
        this.image = image;
        this.videoLink = videoLink;
        this.path = null;
        this.playingState = AppConstants.STATE_NOT_PLAYING;
        this.downloadingState = AppConstants.STATE_NOT_DOWNLOADED;
        this.downloadProgress = 0;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getWatch() {
        return watch;
    }

    public void setWatch(String watch) {
        this.watch = watch;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public int getDownloadingState() {
        return downloadingState;
    }

    public void setDownloadingState(int downloadingState) {
        this.downloadingState = downloadingState;
    }

    public int getPlayingState() {
        return playingState;
    }

    public void setPlayingState(int playingState) {
        this.playingState = playingState;
    }

    public int getDownloadProgress() {
        return downloadProgress;
    }

    public void setDownloadProgress(int downloadProgress) {
        this.downloadProgress = downloadProgress;
    }

}
