package cc.gm.oscarradio.model.events;

import cc.gm.oscarradio.model.SerialDetails;

public class SerialDetailsReceivedEvent {

    private SerialDetails serialDetails;

    public SerialDetails getSerialDetails() {
        return serialDetails;
    }

    public void setSerialDetails(SerialDetails serialDetails) {
        this.serialDetails = serialDetails;
    }

}
