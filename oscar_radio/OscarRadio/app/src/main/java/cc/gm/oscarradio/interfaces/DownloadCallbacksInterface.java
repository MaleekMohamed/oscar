package cc.gm.oscarradio.interfaces;

public interface DownloadCallbacksInterface {

    public void openDownloadedEpisodes();

}
