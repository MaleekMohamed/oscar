package cc.gm.oscarradio.model.handlers;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.SerialDetails;

public class SerialDetailsHandler extends DefaultHandler {

    private final Context mContext;

    private ArrayList<AudioEpisode> audioEpisodes;

    private SerialDetails currentSerialDetails;
    private AudioEpisode currentEpisode;

    private String currentItem;

    private String currentId;
    private String currentParentId;
    private String currentName;
    private String currentCast;
    private String currentSeriesNumber;
    private String currentDirector;
    private String currentWriter;
    private String currentBrief;
    private String currentSeriesLenght;
    private String currentImage;
    private String currentDownloads;
    private String currentLikes;

    private String currentEpisodeParentId;
    private String currentEpisodeParentName;
    private String currentEpisodeId;
    private String currentEpisodeName;
    private String currentEpisodeDownloads;
    private String currentEpisodeWatch;
    private String currentEpisodeImage;
    private String currentEpisodeVideoLink;


    public SerialDetailsHandler(Context context) {
        super();
        audioEpisodes = new ArrayList<AudioEpisode>();
        mContext = context;
    }

    //start of the XML document
    public void startDocument () { Log.i("DataHandler", "Start of XML document");

    }

    //end of the XML document
    public void endDocument () { Log.i("DataHandler", "End of XML document"); }

    //opening element tag
    public void startElement (String uri, String name, String qName, Attributes atts)
    {
        currentItem = qName;

        if(qName.equals(AppConstants.KEY_SERIAL)) {
            currentSerialDetails = new SerialDetails();
        }else if(qName.equals(AppConstants.KEY_EPISODE)) {
            currentEpisode = new AudioEpisode();

        }

    }

    //closing element tag
    public void endElement (String uri, String name, String qName)
    {

        if(qName.equals(AppConstants.KEY_SERIAL)) {

            currentSerialDetails.setId(currentId);
            currentSerialDetails.setParentId(currentParentId);
            currentSerialDetails.setName(currentName);
            currentSerialDetails.setCast(currentCast);
            currentSerialDetails.setSeriesNumber(currentSeriesNumber);
            currentSerialDetails.setDirector(currentDirector);
            currentSerialDetails.setWriter(currentWriter);
            currentSerialDetails.setBrief(currentBrief);
            currentSerialDetails.setSerieslenght(currentSeriesLenght);
            currentSerialDetails.setImage(currentImage);
            currentSerialDetails.setDownloads(currentDownloads);
            currentSerialDetails.setLikes(currentLikes);

            currentSerialDetails.setAudioEpisodes(audioEpisodes);
        } else if(qName.equals(AppConstants.KEY_EPISODE)) {

            currentEpisode.setId(currentEpisodeId);
            currentEpisode.setParentId(currentEpisodeParentId);
            currentEpisode.setParentName(currentEpisodeParentName);
            currentEpisode.setName(currentEpisodeName);
            currentEpisode.setDownloads(currentEpisodeDownloads);
            currentEpisode.setWatch(currentEpisodeWatch);
            currentEpisode.setImage(currentEpisodeImage);
            currentEpisode.setVideoLink(currentEpisodeVideoLink);

            audioEpisodes.add(currentEpisode);
        }

        //handle the end of an element
    }

    //element content
    public void characters (char ch[], int start, int length)
    {
        String currText = "";
        //loop through the character array
        for (int i=start; i<start+length; i++)
        {
            switch (ch[i]) {
                case '\\':
                    break;
                case '"':
                    break;
                case '\n':
                    break;
                case '\r':
                    break;
                case '\t':
                    break;
                default:
                    currText += ch[i];
                    break;
            }
        }

        if (currText==null || currText.isEmpty() || currText.length()<=0) return;

        if(currentItem.equals(AppConstants.KEY_SERIAL_PARENT_SUB_CAT_ID)){

            currentParentId =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_ID)){

            currentId =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_NAME)){
            currentEpisodeParentName = currText;
            currentName =  currText;
        }else if(currentItem.equals(AppConstants.KEY_SERIALS_NUMBER)){

            currentSeriesNumber =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_CAST)){

            currentCast =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_DIRECTOR)){

            currentDirector =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_WRITER)){

            currentWriter =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_BRIEF)){

            currentBrief =  currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIALS_LENGHT)){

            currentSeriesLenght =  currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_IMAGE)) {

            currentImage = currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_DOWNLOADES)) {

            currentDownloads = currText;

        }else if(currentItem.equals(AppConstants.KEY_SERIAL_LIKES)) {

            currentLikes = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_ID)) {

            currentEpisodeId = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_PARENT_SERIAL_ID)) {

            currentEpisodeParentId = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_NAME)) {

            currentEpisodeName = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_DOWNLOADS)) {

            currentEpisodeDownloads = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_WATCH)) {

            currentEpisodeWatch = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_IMAGE)) {

            currentEpisodeImage = currText;

        }else if(currentItem.equals(AppConstants.KEY_EPISODE_VIDEO_LINK)) {

            final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

            String urlEncoded = Uri.encode(currText, ALLOWED_URI_CHARS);

            currentEpisodeVideoLink = urlEncoded;

        }

        currText="";
    }

    public SerialDetails getData(String xml)
    {
        //take care of SAX, input and parsing errors
        try
        {
            //set the parsing driver
            System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");

            //create a parser
            SAXParserFactory parseFactory = SAXParserFactory.newInstance();
            SAXParser xmlParser = parseFactory.newSAXParser();

            //get an XML reader
            XMLReader xmlIn = xmlParser.getXMLReader();

            //instruct the app to use this object as the handler
            xmlIn.setContentHandler(this);

            //Ignore white spaces between elements that causes problem while parsing
            xml = xml.replaceAll(">\\s*<", "><");

            xmlIn.parse(new InputSource(new StringReader(xml)));
        }
        catch(Exception oe) {
            Log.e("AndroidTestsActivity",
                    "Unspecified Error " + oe.getMessage());
        }
        //return the parsed product list
        return currentSerialDetails;
    }
    public Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

}
