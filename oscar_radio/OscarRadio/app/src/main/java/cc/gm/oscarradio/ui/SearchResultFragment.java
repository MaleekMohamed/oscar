package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.SerialAdapter;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.model.events.SearchResultErrorReceivedEvent;
import cc.gm.oscarradio.model.events.SearchResultReceivedEvent;
import cc.gm.oscarradio.model.Serial;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.GetMostDownloadedSerialsRequest;
import cc.gm.oscarradio.webservices.requests.GetMostLikedSerialsRequest;
import cc.gm.oscarradio.webservices.requests.GetMostWatchedSerialsRequest;
import cc.gm.oscarradio.webservices.requests.GetSearchResultRequest;
import de.greenrobot.event.EventBus;

public class SearchResultFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private View rootView;
    private GridView gvSerials;
    private ArrayList<Serial> data;
    private SerialAdapter adapter;
    private int mostType = -1;

    private TextView tvEmptyView;

    private String title;
    private String keyword;

    private ActivityCallbacksInterface mActivityCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.mostType = getArguments().getInt(AppConstants.ARG_MOST_TYPE);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_search_result, container,false);

        initBindViews();

        UIUtils.overrideFonts(getActivity(), rootView);
        //setupCustomActionBar(title);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        setContentShown(false);

        if(mostType != -1)
            getMostSerials(mostType);

    }

    private void getMostSerials(int mostType) {
        switch (mostType) {
            case AppConstants.MOST_DOENLOADED_SERIALS:
                //make request to most serials
                RequestManager.getInstance(getActivity()).doRequest().getMostDownloadedSerials();
                break;
            case AppConstants.MOST_LIKED_SERIALS:
                //make request to most serials
                RequestManager.getInstance(getActivity()).doRequest().getMostLikedSerials();
                break;
            case AppConstants.MOST_WATCHED_SERIALS:
                //make request to most serials
                RequestManager.getInstance(getActivity()).doRequest().getMostWatchedSerials();
                break;
            case AppConstants.SEARCH_SERIALS:
                this.keyword = getArguments().getString(AppConstants.ARG_SEARCH_KEYWORK);
                RequestManager.getInstance(getActivity()).doRequest().getSearchResult(keyword);
                tvEmptyView.setText(getString(R.string.no_search_result));
                break;
            default:
                break;
        }
    }

    private void initBindViews() {
        gvSerials = (GridView)rootView.findViewById(R.id.gv_search_serials);
        tvEmptyView = (TextView) rootView.findViewById(R.id.tv_empty_view);

        gvSerials.setOnItemClickListener(this);

        gvSerials.setEmptyView(rootView.findViewById(R.id.tv_empty_view));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SUB_CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RequestManager.getInstance(getActivity()).doRequest().cancelAllRequestes(AppConstants.REQUEST_SEARCH_TAG);

        EventBus.getDefault().removeStickyEvent(SearchResultReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SearchResultReceivedEvent event){
        mActivityCallbacks.disableRefresh();

        data = event.getData();

        adapter = new SerialAdapter(getActivity(),data);

        gvSerials.setAdapter(adapter);

        setContentShown(true);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SearchResultErrorReceivedEvent event){
        StringRequest request = null;

        switch (mostType) {
            case AppConstants.MOST_DOENLOADED_SERIALS:

                request = new GetMostDownloadedSerialsRequest();

                break;
            case AppConstants.MOST_LIKED_SERIALS:

                request = new GetMostLikedSerialsRequest();

                break;
            case AppConstants.MOST_WATCHED_SERIALS:

                request = new GetMostWatchedSerialsRequest();

                break;
            case AppConstants.SEARCH_SERIALS:

                this.keyword = getArguments().getString(AppConstants.ARG_SEARCH_KEYWORK);

                request = new GetSearchResultRequest(keyword);

                break;
            default:
                break;
        }

        if(request != null) {
            mActivityCallbacks.enableRefresh(request, AppConstants.REQUEST_SEARCH_TAG);
        }

        String errorMessage = event.getErrorMessage();

        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

        setContentShown(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_action_bar_back:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.SERIALS_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.SERIALS_FRAGMENT_ID);

        SerialDetailsFragment serialsFragment = new SerialDetailsFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_SERIAL_ID, data.get(position).getId());
        args.putString(AppConstants.ARG_TITLE, data.get(position).getName());

        serialsFragment.setArguments(args);

        mActivityCallbacks.addFragment(serialsFragment, SerialDetailsFragment.class.getSimpleName());
    }
}
