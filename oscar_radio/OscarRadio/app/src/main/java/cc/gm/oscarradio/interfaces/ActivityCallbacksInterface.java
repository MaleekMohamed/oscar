package cc.gm.oscarradio.interfaces;

import android.support.v4.app.Fragment;

import com.android.volley.toolbox.StringRequest;

public interface ActivityCallbacksInterface {

    public void setupCustomActionBar(String title, int backButtonVisibility, boolean clickable);

    public void addFragment(Fragment targetFragment, String tag);

    public void replaceFragment(Fragment targetFragment, String tag);

    public void doBack();

    public void setItemClickable(boolean clickable, int fragmentId);

    public boolean getItemClickable(int fragmentId);

    public void enableRefresh(StringRequest request, String tag);

    public void disableRefresh();
}
