package cc.gm.oscarradio.model.events;

import java.util.ArrayList;

import cc.gm.oscarradio.model.VideoEpisode;

public class ComingSoonReceivedEvent {

    private ArrayList<VideoEpisode> data;

    public ArrayList<VideoEpisode> getData() {
        return data;
    }

    public void setData(ArrayList<VideoEpisode> data) {
        this.data = data;
    }
}
