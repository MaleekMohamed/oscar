package cc.gm.oscarradio.model.events;

public class ProgressEvent {

    private int progress;

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getProgress() {
        return this.progress;
    }

}
