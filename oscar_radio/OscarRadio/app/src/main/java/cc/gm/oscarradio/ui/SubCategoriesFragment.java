package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.SubCategoryAdapter;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.model.events.SubCategoriesErrorReceivedEvent;
import cc.gm.oscarradio.model.events.SubCategoriesReceivedEvent;
import cc.gm.oscarradio.model.SubCategory;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.GetSubCategoriesRequest;
import de.greenrobot.event.EventBus;

public class SubCategoriesFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View rootView;
    private ListView lvSubCategories;
    private ImageView ivSearch;
    private EditText etSearch;
    private Button btnMostWatched, btnMostDownloaded, btnMostLiked;
    private ArrayList<SubCategory> data;
    private SubCategoryAdapter adapter;
    private String catId;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.catId = getArguments().getString(AppConstants.ARG_CAT_ID);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sub_category, container,false);

        initBindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        setContentShown(false);

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSubCategories(catId);

    }

    private void initBindViews() {
        lvSubCategories = (ListView)rootView.findViewById(R.id.lv_sub_categories);

        ivSearch = (ImageView) rootView.findViewById(R.id.iv_search);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);


        btnMostWatched = (Button) rootView.findViewById(R.id.btn_most_watched);
        btnMostDownloaded = (Button) rootView.findViewById(R.id.btn_most_downloaded);
        btnMostLiked = (Button) rootView.findViewById(R.id.btn_most_liked);

        // Set buttons effect
        UIUtils.setImageClickEffect(ivSearch);

        // Init click Listeners
        ivSearch.setOnClickListener(this);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        btnMostWatched.setOnClickListener(this);
        btnMostDownloaded.setOnClickListener(this);
        btnMostLiked.setOnClickListener(this);

        lvSubCategories.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(SubCategoriesReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SubCategoriesErrorReceivedEvent.class);

        RequestManager.getInstance(OscarApplication.getAppContext())
                .doRequest().cancelAllRequestes(AppConstants.REQUEST_SUB_CATEGORIES_TAG);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SubCategoriesReceivedEvent event){
        mActivityCallbacks.disableRefresh();

        data = event.getData();

        adapter= new SubCategoryAdapter(OscarApplication.getAppContext(),data);

        lvSubCategories.setAdapter(adapter);

        setContentShown(true);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SubCategoriesErrorReceivedEvent event){
        GetSubCategoriesRequest request = new GetSubCategoriesRequest(catId);

        mActivityCallbacks.enableRefresh(request, AppConstants.REQUEST_SUB_CATEGORIES_TAG);

        String errorMessage = event.getErrorMessage();

        Toast.makeText(OscarApplication.getAppContext(), errorMessage, Toast.LENGTH_LONG).show();

        setContentShown(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search:
                search();
                break;
            case R.id.btn_most_watched:
                getMostSerials(AppConstants.MOST_WATCHED_SERIALS, getString(R.string.btn_most_watched));
                break;
            case R.id.btn_most_downloaded:
                getMostSerials(AppConstants.MOST_DOENLOADED_SERIALS, getString(R.string.btn_most_downloaded));
                break;
            case R.id.btn_most_liked:
                getMostSerials(AppConstants.MOST_LIKED_SERIALS, getString(R.string.btn_most_liked));
                break;
            default:
                break;
        }
    }

    private void search() {
        //Toast.makeText(getActivity(), etSearch.getText().toString(), Toast.LENGTH_SHORT).show();
        String searchKeyword = etSearch.getText().toString();

        try {
            searchKeyword = URLEncoder.encode(searchKeyword, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(!UIUtils.isEmpty(searchKeyword)) {
            SearchResultFragment fragment = new SearchResultFragment();

            Bundle args = new Bundle();

            args.putString(AppConstants.ARG_TITLE, getString(R.string.search));
            args.putString(AppConstants.ARG_SEARCH_KEYWORK, searchKeyword);
            args.putInt(AppConstants.ARG_MOST_TYPE, AppConstants.SEARCH_SERIALS);

            fragment.setArguments(args);

            mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
        }

        etSearch.clearFocus();
        InputMethodManager in = (InputMethodManager)OscarApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    private void getMostSerials(int mostType, String title) {
        SearchResultFragment fragment = new SearchResultFragment();

        Bundle args = new Bundle();

        args.putString(AppConstants.ARG_TITLE, title);
        args.putInt(AppConstants.ARG_MOST_TYPE, mostType);

        fragment.setArguments(args);

        mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.SUB_CAT_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.SUB_CAT_FRAGMENT_ID);

        SerialsFragment serialsFragment = new SerialsFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_SUB_CAT_ID,data.get(position).getId());
        args.putString(AppConstants.ARG_TITLE, data.get(position).getName());

        serialsFragment.setArguments(args);

        mActivityCallbacks.addFragment(serialsFragment, SerialsFragment.class.getSimpleName());
    }
}
