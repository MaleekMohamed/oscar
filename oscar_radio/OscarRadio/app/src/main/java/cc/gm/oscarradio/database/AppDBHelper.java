package cc.gm.oscarradio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.Serial;

public class AppDBHelper extends SQLiteOpenHelper {

    // Logcat tag
    //private static final String LOG = "AppDbHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "oscar_db";

    // Table Names
    private static final String TABLE_EPISODES = "episodes";

    // Common column names
    public static final String _ID = "_id";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String CAST = "cast";

    // Episodes table column names
    public static final String PARENT_ID = "parent_id";
    public static final String PARENT_NAME = "parent_name";
    public static final String PATH = "path";
    public static final String VIDEO_LINK= "video_link";

    // Episodes table create statement
    private static final String CREATE_EPISODES = "create table "
            + TABLE_EPISODES
            + "("
            + _ID + " INTEGER PRIMARY KEY, "
            + ID + "  TEXT NOT NULL, "
            + PARENT_ID + "  TEXT NOT NULL, "
            + NAME + " TEXT NOT NULL, "
            + PARENT_NAME + " TEXT NOT NULL, "
            + PATH + " TEXT, "
            + VIDEO_LINK + " TEXT NOT NULL, "
            + CAST + " TEXT"
            + ");";

    private String[] episodeColumns = {_ID, ID, PARENT_ID,
            NAME, PARENT_NAME, PATH, VIDEO_LINK, CAST};

    private static final String TABLE_SERIALS = "serials";
    // Serials table column names
    public static final String IMAGE = "image";

    // Serials table create statement
    private static final String CREATE_SERIALS = "create table "
            + TABLE_SERIALS
            + "("
            + _ID + " INTEGER PRIMARY KEY, "
            + ID + "  TEXT NOT NULL, "
            + NAME + " TEXT NOT NULL, "
            + CAST + " TEXT NOT NULL, "
            + IMAGE + " TEXT NOT NULL"
            + ");";

    private String[] serialColumns = {_ID, ID, NAME, CAST, IMAGE};

    public AppDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_EPISODES);
        db.execSQL(CREATE_SERIALS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EPISODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERIALS);
        onCreate(db);
    }

    public boolean insertEpisode(String id, String parentId, String name, String parentName,
                                 String path, String videoLink, String cast) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID, id);
        values.put(PARENT_ID, parentId);
        values.put(NAME, name);
        values.put(PARENT_NAME, parentName);
        values.put(PATH, path);
        values.put(VIDEO_LINK, videoLink);
        values.put(CAST, cast);

        long insertId = database.insert(TABLE_EPISODES, null,
                values);

        database.close();

        if(insertId == -1)
            return false;
        else
            return true;
    }

    public boolean insertOrUpdateEpisode(String id, String parentId, String name, String parentName,
                                         String path, String videoLink, String cast) {
        SQLiteDatabase database	= getWritableDatabase();

        String selection = ID + " = ?" + " AND " + PARENT_ID + " = ?";
        String[] selectionArgs = {id, parentId};

        ContentValues values = new ContentValues();
        values.put(PATH, path);

        int updatedRows = database.update(TABLE_EPISODES, values, selection, selectionArgs);

        database.close();

        if(updatedRows > 0) {
            return true;
        } else {
            return insertEpisode(id, parentId, name, parentName, path, videoLink, cast);
        }

    }

    public boolean deleteEpisode(String id, String parentId) {
        SQLiteDatabase database = getWritableDatabase();

        String selection = ID + " = ?" + " AND " + PARENT_ID + " = ?";
        String[] selectionArgs = {id, parentId};

        long deleteId = database.delete(TABLE_EPISODES, selection, selectionArgs);

        database.close();
        if(deleteId == -1)
            return false;
        else
            return true;
    }

    public boolean insertSerial(Serial serial) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID, serial.getId());
        values.put(NAME, serial.getName());
        values.put(CAST, serial.getCast());
        values.put(IMAGE, serial.getImage());

        long insertId = database.insert(TABLE_SERIALS, null,
                values);

        database.close();

        if(insertId == -1)
            return false;
        else
            return true;
    }

    public ArrayList<AudioEpisode> getEpisodes () {
        SQLiteDatabase database = getReadableDatabase();

        ArrayList<AudioEpisode> episodes = new ArrayList<AudioEpisode>();

        Cursor cursor = database.query(TABLE_EPISODES, null, null,
                null, null, null, _ID + " DESC", null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            AudioEpisode episode = cursorToEpisode(cursor);

            episodes.add(episode);

            cursor.moveToNext();

        }

        cursor.close();

        database.close();

        return episodes;
    }

    public ArrayList<Serial> getSerials () {
        SQLiteDatabase database = getReadableDatabase();

        ArrayList<Serial> serials = new ArrayList<Serial>();

//        Cursor cursor = database.query(TABLE_SERIALS, serialColumns, null,
//                null, null, null, _ID + " DESC", null);

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_SERIALS, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Serial serial = cursorToSerial(cursor);

            serials.add(serial);

            cursor.moveToNext();

        }

        cursor.close();

        database.close();

        return serials;
    }

    public AudioEpisode getEpisodeById(String id) {

        SQLiteDatabase database = getReadableDatabase();

        AudioEpisode episode = null;

        String selection = ID + " = ?";

        String[] selectionArgs = {id};

        Cursor cursor = database.query(TABLE_EPISODES, episodeColumns,
                selection, selectionArgs, null, null, null);

        if(cursor.moveToFirst()) {
            cursor.moveToFirst();
            episode = cursorToEpisode(cursor);
        }
        else {
            return null;
        }

        cursor.close();

        database.close();

        return episode;
    }

    public boolean removeSerialFromFavorites(String id) {
        SQLiteDatabase database = getWritableDatabase();

        String selection = ID + " = ?";
        String[] selectionArgs = {id};

        long deleteId = database.delete(TABLE_SERIALS, selection, selectionArgs);

        database.close();

        if(deleteId == -1)
            return false;
        else
            return true;
    }

    public Serial getSerialById(String id) {

        SQLiteDatabase database = getReadableDatabase();

        Serial serial = null;

        String selection = ID + " = ?";

        String[] selectionArgs = {id};

//        Cursor cursor = database.query(TABLE_SERIALS, serialColumns,
//                selection, selectionArgs, null, null, null);

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_SERIALS + " WHERE " + ID + "=" + id , null);

        if(cursor.moveToFirst()) {
            cursor.moveToFirst();
            serial = cursorToSerial(cursor);
        }
        else {
            return null;
        }

        cursor.close();

        database.close();

        return serial;
    }

    private AudioEpisode cursorToEpisode(Cursor cursor) {
        AudioEpisode episode = new AudioEpisode();

        episode.setId(cursor.getString(1));
        episode.setParentId(cursor.getString(2));
        episode.setName(cursor.getString(3));
        episode.setParentName(cursor.getString(4));
        episode.setPath(cursor.getString(5));
        episode.setVideoLink(cursor.getString(6));
        episode.setCast(cursor.getString(7));

        return episode;
    }

    private Serial cursorToSerial(Cursor cursor) {
        Serial serial = new Serial();

        serial.setId(cursor.getString(1));
        serial.setName(cursor.getString(2));
        serial.setCast(cursor.getString(3));
        serial.setImage(cursor.getString(4));

        return serial;
    }
}