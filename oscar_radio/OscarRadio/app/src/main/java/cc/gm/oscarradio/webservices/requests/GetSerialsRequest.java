package cc.gm.oscarradio.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.Serial;
import cc.gm.oscarradio.model.events.SerialErrorReceivedEvent;
import cc.gm.oscarradio.model.handlers.SerialHandler;
import cc.gm.oscarradio.model.events.SerialReceivedEvent;
import cc.gm.oscarradio.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class GetSerialsRequest extends StringRequest {

    public GetSerialsRequest(String subCatId) {
        super(Method.GET,
                AppConstants.GET_SERIALS_URL + subCatId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        ArrayList<Serial> data = new SerialHandler(OscarApplication.getAppContext()).getData(response);

                        if(data.size() > 0) {
                            SerialReceivedEvent event = new SerialReceivedEvent();
                            event.setData(data);

                            EventBus.getDefault().postSticky(event);
                        } else {
                            SerialErrorReceivedEvent event = new SerialErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.generic_error));

                            EventBus.getDefault().postSticky(event);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        SerialErrorReceivedEvent event = new SerialErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public GetSerialsRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));


    }
}