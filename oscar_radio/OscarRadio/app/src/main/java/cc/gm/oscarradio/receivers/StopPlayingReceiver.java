package cc.gm.oscarradio.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.backgroundtasks.MusicService;

public class StopPlayingReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if(MusicService.isRunning()) {
            Intent serviceIntent = new Intent(context, MusicService.class);
            serviceIntent.putExtra(AppConstants.EXTRA_SERVICE_ACTION, intent.getAction());
            context.startService(serviceIntent);
        }

    }
}
