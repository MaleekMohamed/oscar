package cc.gm.oscarradio;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;

import cc.gm.oscarradio.backgroundtasks.MusicService;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.interfaces.DownloadCallbacksInterface;
import cc.gm.oscarradio.interfaces.MusicCallbacksInterface;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.events.EpisodePlayingStateEvent;
import cc.gm.oscarradio.model.events.WatchedReceviedEvent;
import cc.gm.oscarradio.ui.DownloadedEpisodesFragment;
import cc.gm.oscarradio.ui.FavoriteSerialsFragment;
import cc.gm.oscarradio.ui.MainFragment;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.WatchedRequest;
import de.greenrobot.event.EventBus;


public class MainActivity extends ActionBarActivity implements ActivityCallbacksInterface, OnClickListener,
        MusicCallbacksInterface, DownloadCallbacksInterface{

    private static  int transactionState=-1;

    private StringRequest mCurrentRequest;

    private ActionBar mCustomActionBar;
    private View mCustomActionBarView;
    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack;
    private ProgressBar pbActionBarRefresh;
    private ImageView ivActionBarLogo;
    private ImageView ivActionBarRefresh;
    private TextView tvActionBarTitle;

    private LinearLayout llBottomBar, llPlayBar;
    private ImageView ivDownloads, ivFavorites, /*ivAll,*/ ivPlayBarPause, ivPlayBarClose;
    private TextView tvPlayBarEpisodeName;

    private boolean catItemClickable = true;
    private boolean subCatItemClickable = true;
    private boolean seriesItemClickable = true;
    private boolean specialSeriesItemClickable = true;
    private boolean favoriteSerialsItemClickable = true;
    private boolean searchSerialsItemClickable = true;

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    private AudioEpisode episode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupCustomActionBar();

        MusicService.cancelNotification();

        playIntent = new Intent(this, MusicService.class);

        mFragmentManager = getSupportFragmentManager();

        initBindViews();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new MainFragment(), MainFragment.class.getSimpleName())
                    .commit();
        }

        EventBus.getDefault().register(this);

        if(isMyServiceRunning(MusicService.class)){
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void initBindViews() {
        llBottomBar = (LinearLayout) findViewById(R.id.ll_main_bottom_bar);
        llPlayBar = (LinearLayout) findViewById(R.id.ll_main_bottom_play_bar);

        ivDownloads = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_downloads);
        ivFavorites = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_favorites);
        //ivAll = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_all);

        ivPlayBarPause = (ImageView) llPlayBar.findViewById(R.id.iv_main_play_bar_pause);
        ivPlayBarClose = (ImageView) llPlayBar.findViewById(R.id.iv_main_play_bar_close);

        tvPlayBarEpisodeName = (TextView) llPlayBar.findViewById(R.id.tv_main_play_bar_name);

        UIUtils.setImageClickEffect(ivDownloads);
        UIUtils.setImageClickEffect(ivFavorites);
        //UIUtils.setImageClickEffect(ivAll);

        UIUtils.overrideFonts(this, tvPlayBarEpisodeName);

        UIUtils.setImageClickEffect(ivPlayBarPause);
        UIUtils.setImageClickEffect(ivPlayBarClose);

        ivDownloads.setOnClickListener(this);
        ivFavorites.setOnClickListener(this);
        //ivAll.setOnClickListener(this);

        ivPlayBarPause.setOnClickListener(this);
        ivPlayBarClose.setOnClickListener(this);
    }

    private void setupCustomActionBar() {
        mCustomActionBar = getSupportActionBar();

        mCustomActionBar.setDisplayShowTitleEnabled(false);
        mCustomActionBar.setDisplayUseLogoEnabled(false);
        mCustomActionBar.setDisplayHomeAsUpEnabled(false);
        mCustomActionBar.setDisplayShowHomeEnabled(false);
        mCustomActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mCustomActionBar.setDisplayShowCustomEnabled(true);


        mCustomActionBar.setCustomView(R.layout.custom_action_bar);

        mCustomActionBarView = mCustomActionBar.getCustomView();

        llActionBarUp = (LinearLayout) mCustomActionBarView.findViewById(R.id.ll_action_bar_up);
        ivActionBarBack = (ImageView) mCustomActionBarView.findViewById(R.id.iv_action_bar_back);
        ivActionBarLogo = (ImageView) mCustomActionBarView.findViewById(R.id.iv_action_bar_logo);
        ivActionBarRefresh = (ImageView) mCustomActionBarView.findViewById(R.id.iv_action_bar_refresh);
        pbActionBarRefresh = (ProgressBar) mCustomActionBarView.findViewById(R.id.pb_action_bar_refresh);
        tvActionBarTitle = (TextView) mCustomActionBarView.findViewById(R.id.tv_action_bar_title);

        llActionBarUp.setOnClickListener(this);

        ivActionBarRefresh.setOnClickListener(this);

        UIUtils.setImageClickEffect(ivActionBarRefresh);

        UIUtils.overrideFonts(this, tvActionBarTitle);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        OscarApplication.setMainActivityStateSaved(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (transactionState == 0) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new MainFragment())
                    .commit();
        }

    }

    @Override
    public void setupCustomActionBar(String title, int backButtonVisibility, boolean clickable) {
        if(ivActionBarBack != null)
            ivActionBarBack.setVisibility(backButtonVisibility);

        if(tvActionBarTitle != null)
            tvActionBarTitle.setText(title);

        if(llActionBarUp != null) {
            if(clickable) {
                llActionBarUp.setOnClickListener(this);
                llActionBarUp.setBackgroundResource(R.drawable.list_item_effect);
            } else {
                llActionBarUp.setOnClickListener(null);
                llActionBarUp.setBackgroundResource(R.color.transparent);
            }
        }
    }

    @Override
    public void addFragment(Fragment targetFragment, String tag) {
        if(mFragmentManager != null) {
            mFragmentTransaction = mFragmentManager.beginTransaction();

            mFragmentTransaction.add(R.id.container, targetFragment, tag);

            mFragmentTransaction.addToBackStack(null);

            mFragmentTransaction.commit();
        }
    }

    @Override
    public void replaceFragment(Fragment targetFragment, String tag) {
        if(mFragmentManager != null) {
            mFragmentTransaction = mFragmentManager.beginTransaction();

            mFragmentTransaction.replace(R.id.container, targetFragment, tag);

            mFragmentTransaction.addToBackStack(null);

            mFragmentTransaction.commit();
        }
    }

    @Override
    public void doBack() {
        onBackPressed();
    }



    @Override
    public void setItemClickable(boolean clickable, int fragmentId) {
        switch (fragmentId) {
            case AppConstants.CAT_FRAGMENT_ID:
                catItemClickable = clickable;
                break;
            case AppConstants.SUB_CAT_FRAGMENT_ID:
                subCatItemClickable = clickable;
                break;
            case AppConstants.SERIALS_FRAGMENT_ID:
                seriesItemClickable = clickable;
                break;
            case AppConstants.FAVORITE_SERIALS_FRAGMENT_ID:
                favoriteSerialsItemClickable = clickable;
                break;
            case AppConstants.SEARCH_FRAGMENT_ID:
                searchSerialsItemClickable = clickable;
                break;
            case AppConstants.SPECIAL_SERIALS_FRAGMENT_ID:
                specialSeriesItemClickable = clickable;
                break;
            default:
                break;
        }
    }

    @Override
    public boolean getItemClickable(int fragmentId) {
        switch (fragmentId) {
            case AppConstants.CAT_FRAGMENT_ID:
                return catItemClickable;
            case AppConstants.SUB_CAT_FRAGMENT_ID:
                return subCatItemClickable;
            case AppConstants.SERIALS_FRAGMENT_ID:
                return seriesItemClickable;
            case AppConstants.FAVORITE_SERIALS_FRAGMENT_ID:
                return favoriteSerialsItemClickable;
            case AppConstants.SEARCH_FRAGMENT_ID:
                return searchSerialsItemClickable;
            case AppConstants.SPECIAL_SERIALS_FRAGMENT_ID:
                return specialSeriesItemClickable;
            default:
                return true;
        }
    }

    @Override
    public void enableRefresh(StringRequest request, String tag) {
        this.mCurrentRequest = request;

        mCurrentRequest.setTag(tag);

        if(ivActionBarRefresh != null) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
        }

        if(pbActionBarRefresh != null) {
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void disableRefresh() {
        this.mCurrentRequest = null;

        if(ivActionBarRefresh != null) {
            ivActionBarRefresh.setVisibility(View.GONE);
        }

        if(pbActionBarRefresh != null) {
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_action_bar_up:
                onBackPressed();
                break;
            case R.id.iv_bottom_bar_downloads:
                getDownloads();
                break;
            case R.id.iv_bottom_bar_favorites:
                getFavorites();
                break;
            case R.id.iv_main_play_bar_close:
                stopEpisode();
                break;
            case R.id.iv_main_play_bar_pause:
                switch (MusicService.getEpisode().getPlayingState()) {
                    case AppConstants.STATE_PLAYING:
                        pauseEpisode();
                        break;
                    case AppConstants.STATE_PAUSED:
                        resumeEpisode();
                        break;
                    case AppConstants.STATE_NOT_PLAYING:
                        if(episode != null) {
                            playEpisode(episode);
                        }
                    case AppConstants.STATE_COMPLETED:
                        if(episode != null) {
                            playEpisode(episode);
                        }
                    default:
                        break;
                }
                break;
            case R.id.iv_action_bar_refresh:
                refreshLastRequest();
                break;
            default:
                break;
        }
    }

    private void refreshLastRequest() {
        //make request to main categories
        RequestManager.getInstance(this).doRequest().addRequest(mCurrentRequest);

        if(ivActionBarRefresh != null) {
            ivActionBarRefresh.setVisibility(View.GONE);
        }

        if(pbActionBarRefresh != null) {
            pbActionBarRefresh.setVisibility(View.VISIBLE);
        }
    }

    private void getDownloads() {
        DownloadedEpisodesFragment downloadedFragment;

        downloadedFragment = (DownloadedEpisodesFragment)
                getSupportFragmentManager().findFragmentByTag(DownloadedEpisodesFragment.class.getSimpleName());

        FavoriteSerialsFragment favoriteFragment;

        favoriteFragment = (FavoriteSerialsFragment)
                getSupportFragmentManager().findFragmentByTag(FavoriteSerialsFragment.class.getSimpleName());

        if(favoriteFragment !=null)
            getSupportFragmentManager().beginTransaction().detach(favoriteFragment).commit();

        if (downloadedFragment != null) {
            getSupportFragmentManager().beginTransaction().attach(downloadedFragment).commit();
            return;
        }


        downloadedFragment = new DownloadedEpisodesFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_TITLE, getString(R.string.downloaded_episodes));

        downloadedFragment.setArguments(args);

        addFragment(downloadedFragment, DownloadedEpisodesFragment.class.getSimpleName());
    }

    private void getFavorites() {

        DownloadedEpisodesFragment downloadedFragment;

        downloadedFragment = (DownloadedEpisodesFragment)
                getSupportFragmentManager().findFragmentByTag(DownloadedEpisodesFragment.class.getSimpleName());


        FavoriteSerialsFragment favoriteFragment;

        favoriteFragment = (FavoriteSerialsFragment)
                getSupportFragmentManager().findFragmentByTag(FavoriteSerialsFragment.class.getSimpleName());

        if(downloadedFragment !=null)
            getSupportFragmentManager().beginTransaction().detach(downloadedFragment).commit();

        if(favoriteFragment !=null) {
            getSupportFragmentManager().beginTransaction().attach(favoriteFragment).commit();
            return;
        }


        favoriteFragment = new FavoriteSerialsFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_TITLE, getString(R.string.favorite));

        favoriteFragment.setArguments(args);

        addFragment(favoriteFragment, FavoriteSerialsFragment.class.getSimpleName());
    }

    private MusicService musicSrv;
    private Intent playIntent;
    private boolean musicBound=false;
    private WatchedRequest watchedRequest;
    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder)service;
            //get service
            musicSrv = binder.getService();
            //pass list
            musicBound = true;

            if(episode == null) {
                episode = musicSrv.getEpisode();

                if(MusicService.getEpisode().getPlayingState() == AppConstants.STATE_PLAYING) {
                    ivPlayBarPause.setImageResource(R.drawable.ic_pause);
                } else {
                    ivPlayBarPause.setImageResource(R.drawable.ic_play);
                }
            } else {
                musicSrv.setEpisode(episode);

                musicSrv.playEpisode();

                ivPlayBarPause.setImageResource(R.drawable.ic_pause);

                RequestManager.getInstance(MainActivity.this).doRequest().doWatched(episode.getParentId());
            }

            llPlayBar.setVisibility(View.VISIBLE);
            llBottomBar.setVisibility(View.GONE);

            tvPlayBarEpisodeName.setText(episode.getName());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    @Override
    public void playEpisode(AudioEpisode episode) {

        this.episode = episode;



        if(!isMyServiceRunning(MusicService.class)){
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        } else {
            if (musicSrv != null) {
                musicSrv.setEpisode(episode);
                musicSrv.playEpisode();

                llPlayBar.setVisibility(View.VISIBLE);
                llBottomBar.setVisibility(View.GONE);

                ivPlayBarPause.setImageResource(R.drawable.ic_pause);
                tvPlayBarEpisodeName.setText(episode.getName());

//                watchedRequest = new WatchedRequest(episode.getParentId());
//                watchedRequest.start();

                RequestManager.getInstance(this).doRequest().doWatched(episode.getParentId());
            }
        }
    }

    @Override
    public void stopEpisode() {
        if(llPlayBar != null) {
            llPlayBar.setVisibility(View.GONE);
        }

        if(llBottomBar != null) {
            llBottomBar.setVisibility(View.VISIBLE);
        }

        if(MusicService.isRunning()) {
            if(musicSrv != null) {
                musicSrv.stopEpisode();
            }

            unbindService(musicConnection);
            stopService(playIntent);
        }
    }

    @Override
    public void pauseEpisode() {
        if(MusicService.isRunning()) {
            if (musicSrv != null) {
                musicSrv.pauseEpisode();
            }

            ivPlayBarPause.setImageResource(R.drawable.ic_play);
            //tvPlayBarEpisodeName.setText(episode.getName());
        }
    }

    @Override
    public void resumeEpisode() {
        if(MusicService.isRunning()) {
            if (musicSrv != null) {
                musicSrv.resumeEpisode();
            }

            ivPlayBarPause.setImageResource(R.drawable.ic_pause);
            //tvPlayBarEpisodeName.setText(episode.getName());
        }
    }

    public void onEvent(EpisodePlayingStateEvent event){
        switch (event.getEpisode().getPlayingState()) {
            case AppConstants.STATE_COMPLETED:
                if(ivPlayBarPause != null) {
                    ivPlayBarPause.setImageResource(R.drawable.ic_play);
                }
                break;
            default:
                break;
        }
    }

    public void onEventMainThread(WatchedReceviedEvent event) {
        //Toast.makeText(this, event.getStatus(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(isMyServiceRunning(MusicService.class)) {
            if(episode == null) {
                episode = musicSrv.getEpisode();
            }

            if(musicSrv != null)
                musicSrv.showNotification();
        }

        try {
            unbindService(musicConnection);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().unregister(this);
        EventBus.getDefault().removeAllStickyEvents();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void openDownloadedEpisodes() {
        getDownloads();
    }

}
