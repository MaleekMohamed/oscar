package cc.gm.oscarradio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.MainCategory;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.views.RoundedCornersImageView;

public class MainCategoryAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<MainCategory> catList;

    public MainCategoryAdapter(Context context, List<MainCategory> data) {
        this.mContext = context;
        this.catList = data;
    }

    @Override
    public int getCount() {
        return catList.size();
    }

    @Override
    public Object getItem(int position) {
        return catList.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(catList.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rootView = inflater.inflate(R.layout.adapter_main_category,null);

        MainCategory item = catList.get(position);

        final RoundedCornersImageView catIcon = (RoundedCornersImageView) rootView.findViewById(R.id.iv_main_cat_icon);

        TextView catName = (TextView) rootView.findViewById(R.id.txt_main_cat_name);

        if(UIUtils.isEmpty(item.getIcon())) {
            catIcon.setImageResource(R.drawable.default_image);
        } else {
            Picasso.with(mContext).load(item.getIcon()).into(catIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    catIcon.setImageResource(R.drawable.default_image);
                }
            });
        }

        catName.setText(item.getName());

        UIUtils.overrideFonts(mContext, rootView);

        return rootView;
    }
}
