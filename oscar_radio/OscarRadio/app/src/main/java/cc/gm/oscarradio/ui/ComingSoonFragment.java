package cc.gm.oscarradio.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.adapters.ComingSoonAdapter;
import cc.gm.oscarradio.interfaces.ActivityCallbacksInterface;
import cc.gm.oscarradio.interfaces.DownloadCallbacksInterface;
import cc.gm.oscarradio.interfaces.MusicCallbacksInterface;
import cc.gm.oscarradio.model.AudioEpisode;
import cc.gm.oscarradio.model.VideoEpisode;
import cc.gm.oscarradio.model.events.ComingSoonErrorReceivedEvent;
import cc.gm.oscarradio.model.events.DownladVideoEpisodeErrorEvent;
import cc.gm.oscarradio.model.events.VideoEpisodeReceivedEvent;
import cc.gm.oscarradio.model.events.ComingSoonReceivedEvent;
import cc.gm.oscarradio.utils.UIUtils;
import cc.gm.oscarradio.utils.Utils;
import cc.gm.oscarradio.webservices.RequestManager;
import cc.gm.oscarradio.webservices.requests.GetComingSoonRequest;
import de.greenrobot.event.EventBus;

public class ComingSoonFragment extends ProgressFragment implements ComingSoonAdapter.AdapterCallback {

    private View rootView;
    private ListView lvEpisodes;
    private ArrayList<VideoEpisode> episodes;
    private ComingSoonAdapter adapter;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;
    private MusicCallbacksInterface mMusicCallbacks;
    private DownloadCallbacksInterface mDownloadCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
            mMusicCallbacks = (MusicCallbacksInterface) activity;
            mDownloadCallbacks = (DownloadCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_coming_soon, container,false);

        initBindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return rootView;
    }

    private void initBindViews() {
        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_coming_soon_episodes);

        lvEpisodes.setEmptyView(rootView.findViewById(R.id.tv_empty_view));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentShown(false);

        mActivityCallbacks.setupCustomActionBar(title, View.VISIBLE, true);

        episodes = new ArrayList<VideoEpisode>();

        adapter = new ComingSoonAdapter(OscarApplication.getAppContext(), episodes, this);

        lvEpisodes.setAdapter(adapter);

        setContentShown(false);

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getComingSoon();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RequestManager.getInstance(getActivity()).doRequest().cancelAllRequestes(AppConstants.REQUEST_COMING_SOON_TAG);

        EventBus.getDefault().removeStickyEvent(ComingSoonErrorReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(ComingSoonReceivedEvent.class);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(ComingSoonErrorReceivedEvent event){
        GetComingSoonRequest request = new GetComingSoonRequest();

        mActivityCallbacks.enableRefresh(request, AppConstants.REQUEST_COMING_SOON_TAG);

        String errorMessage = event.getErrorMessage();

        Toast.makeText(OscarApplication.getAppContext(), errorMessage, Toast.LENGTH_LONG).show();

        setContentShown(true);
    }

    public void onEventMainThread(ComingSoonReceivedEvent event) {
        mActivityCallbacks.disableRefresh();

        episodes.clear();
        episodes.addAll(event.getData());

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);
    }

    public void onEventMainThread(VideoEpisodeReceivedEvent event) {
        int position = Utils.getItemPosition(event.getEpisode(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(event.getEpisode().getPath());
            episodes.get(position).setDownloadingState(event.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(event.getEpisode().getDownloadProgress());

            adapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(DownladVideoEpisodeErrorEvent errorEvent) {
        int position = Utils.getItemPosition(errorEvent.getEpisode(), episodes);

        if(position != -1) {

            episodes.get(position).setPath(errorEvent.getEpisode().getPath());
            episodes.get(position).setDownloadingState(errorEvent.getEpisode().getDownloadingState());
            episodes.get(position).setDownloadProgress(0);

            adapter.notifyDataSetChanged();
        }

        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void pauseEpisode() {
        mMusicCallbacks.pauseEpisode();
    }

    @Override
    public void playEpisode(AudioEpisode episode) {
        mMusicCallbacks.playEpisode(episode);
    }

    @Override
    public void stopEpisode() {
        mMusicCallbacks.stopEpisode();
    }

    @Override
    public void resumeEpisode() {
        mMusicCallbacks.resumeEpisode();
    }

    @Override
    public void openDownloadedEpisodes() {
        mDownloadCallbacks.openDownloadedEpisodes();
    }

    @Override
    public void playVideoEpisode(VideoEpisode episode) {
        mMusicCallbacks.stopEpisode();

        Intent intent = new Intent(OscarApplication.getAppContext(), VideoPlayerActivity.class);

        intent.putExtra(AppConstants.EXTRA_VIDEO_PATH, episode.getPath());

        startActivity(intent);
    }
}
