package cc.gm.oscarradio.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cc.gm.oscarradio.AppConstants;
import cc.gm.oscarradio.OscarApplication;
import cc.gm.oscarradio.R;
import cc.gm.oscarradio.model.events.MainCategoriesErrorReceivedEvent;
import cc.gm.oscarradio.model.handlers.MainCategoryHandler;
import cc.gm.oscarradio.model.events.MainCategoriesReceivedEvent;
import cc.gm.oscarradio.model.MainCategory;
import cc.gm.oscarradio.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class GetAllCategoriesRequest extends StringRequest {
    public GetAllCategoriesRequest() {
        super(Method.GET,
                AppConstants.GET_ALL_CATS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        ArrayList<MainCategory> data = new MainCategoryHandler(OscarApplication.getAppContext()).getData(response);

                        if(data.size() > 0) {
                            MainCategoriesReceivedEvent event = new MainCategoriesReceivedEvent();
                            event.setData(data);

                            EventBus.getDefault().postSticky(event);
                        } else {
                            MainCategoriesErrorReceivedEvent event = new MainCategoriesErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.generic_error));

                            EventBus.getDefault().postSticky(event);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        MainCategoriesErrorReceivedEvent event = new MainCategoriesErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public GetAllCategoriesRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));
    }
}
